/****************************************************************/
/*********************CURSORES PARA UPDATES**********************/
/****************************************************************/

DECLARE
    CURSOR cur_dept IS
    SELECT
        department_id,
        department_name
    FROM
        departments
    ORDER BY
        department_name;

    CURSOR cur_emp (
        p_deptid NUMBER
    ) IS
    SELECT
        first_name,
        last_name
    FROM
        employees
    WHERE
        department_id = p_deptid
    ORDER BY
        last_name;

    v_deptrec  cur_dept%rowtype;
    v_emprec   cur_emp%rowtype;
BEGIN
    OPEN cur_dept;
    LOOP
        FETCH cur_dept INTO v_deptrec;
        EXIT WHEN cur_dept%notfound;
        dbms_output.put_line( 'NOMBRE DPTO '|| v_deptrec.department_name);
        OPEN cur_emp(v_deptrec.department_id);
        LOOP
            FETCH cur_emp INTO v_emprec;
            EXIT WHEN cur_emp%notfound;
            dbms_output.put_line(v_emprec.last_name
                                 || ' '
                                 || v_emprec.first_name);
        END LOOP;

        CLOSE cur_emp;
    END LOOP;

    CLOSE cur_dept;
END;



/**----------------------------------------------------------*/
DECLARE
    CURSOR cur_loc IS
    SELECT
        *
    FROM
        locations;

    CURSOR cur_dept (
        p_locid NUMBER
    ) IS
    SELECT
        *
    FROM
        departments
    WHERE
        location_id = p_locid;

    v_locrec   cur_loc%rowtype;
    v_deptrec  cur_dept%rowtype;
BEGIN
    OPEN cur_loc;
    LOOP
        FETCH cur_loc INTO v_locrec;
        EXIT WHEN cur_loc%notfound;
        dbms_output.put_line(v_locrec.city);
        OPEN cur_dept(v_locrec.location_id);
        LOOP
            FETCH cur_dept INTO v_deptrec;
            EXIT WHEN cur_dept%notfound;
            dbms_output.put_line(v_deptrec.department_name);
        END LOOP;

        CLOSE cur_dept;
    END LOOP;

    CLOSE cur_loc;
END;


/*----------------------------------------------*/
DECLARE
    CURSOR cur_loc IS
    SELECT
        *
    FROM
        locations;

    CURSOR cur_dept (
        p_locid NUMBER
    ) IS
    SELECT
        *
    FROM
        departments
    WHERE
        location_id = p_locid;

BEGIN
    FOR v_locrec IN cur_loc LOOP
        dbms_output.put_line('CIUDAD: ' || v_locrec.city);
        FOR v_deptrec IN cur_dept(v_locrec.location_id) LOOP
            dbms_output.put_line(;;||v_deptrec.department_name);
        END LOOP;

    END LOOP;
END;

/*-----------------------------------------*/
/*ACTUALIZA EL SALARIO EN 10 % DE LA TABLA EMPLEADOS PERO SOLO EN 
LA LOCACION DONDE DEL DEPARTAMENTO ES EL ID 1700 Y EL SALARIO DE ESE 
*/
DECLARE
    CURSOR cur_dept IS
    SELECT
        *
    FROM
        my_departments;

    CURSOR cur_emp (
        p_dept_id NUMBER
    ) IS
    SELECT
        *
    FROM
        my_employees
    WHERE
        department_id = p_dept_id
    FOR UPDATE NOWAIT;

BEGIN
    FOR v_deptrec IN cur_dept LOOP
        dbms_output.put_line(v_deptrec.department_name);
        FOR v_emprec IN cur_emp(v_deptrec.department_id) LOOP
            dbms_output.put_line(v_emprec.last_name);
            IF
                v_deptrec.location_id = 1700
                AND v_emprec.salary < 10000
            THEN
                UPDATE my_employees
                SET
                    salary = salary * 1.1
                WHERE
                    CURRENT OF cur_emp;

            END IF;

        END LOOP;

    END LOOP;
END;