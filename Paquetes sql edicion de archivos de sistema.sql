BEGIN
DBMS_OUTPUT.PUT('The cat sat ');
DBMS_OUTPUT.PUT('on the mat');
DBMS_OUTPUT.NEW_LINE;
END;

BEGIN
DBMS_OUTPUT.PUT_LINE('The cat sat on the mat');
END;


/*-------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE sal_status (
    p_dir       IN  VARCHAR2,
    p_filename  IN  VARCHAR2
) IS

    v_file       utl_file.file_type;
    CURSOR empc IS
    SELECT
        last_name,
        salary,
        department_id
    FROM
        employees
    ORDER BY
        department_id;

    v_newdeptno  employees.department_id%TYPE;
    v_olddeptno  employees.department_id%TYPE := 0;
BEGIN
    v_file := utl_file.fopen(p_dir, p_filename, 'a'); /*-- 1*/
    utl_file.put_line(v_file,'REPORT: GENERATED ON ' || sysdate);
    utl_file.new_line(v_file);/*GENERA UNA NUEVA LINEA EN EL ARCHIVO*/
    FOR emp_rec IN empc LOOP
        utl_file.put_line(v_file, 
         ' EMPLOYEE: '
                                  || emp_rec.last_name
                                  || 'earns: '
                                  || emp_rec.salary);
    END LOOP;

    utl_file.put_line(v_file, '*** END OF REPORT ***'); 
    utl_file.fclose(v_file); 
EXCEPTION
    WHEN utl_file.invalid_filehandle THEN 
        raise_application_error(-20001, 'Invalid File.');
    WHEN utl_file.write_error THEN 
        raise_application_error(-20002, 'Unable to 
write to file');
END sal_status;



create or replace directory OUT_DIR as 'C:\OUT_DIR';

select * from all_directories
where  directory_name = 'TEMP_DIR1';

BEGIN
    sal_status('PRUEBA1', 'u12345.txt');
END;

SELECT *
FROM dba_directories;

SELECT grantor, grantee, table_schema, table_name, privilege
FROM all_tab_privs
WHERE table_name = 'CTEMP';

SELECT * FROM all_tab_privs WHERE grantee = 'PUBLIC' AND TABLE_NAME = 'UTL_FILE';












utl_mail.send ( sender IN VARCHAR2,
recipients IN VARCHAR2,
subject IN VARCHAR2 DEFAULT NULL,
message IN VARCHAR2);



BEGIN
UTL_MAIL.SEND('angellm9604@gmail.com',
'mlopezmejia81@gmail.com',
message => 'Friday�s meeting will be at 10:30 in
Room 6',
subject => 'Our PL/SQL meeting');
END;