/**********************************************************************************/
/*******************************TRIGGERS*******************************************/
/**********************************************************************************/
/*CREAMOS UNA TABLA QUE ALMACENA LOS DATOS DE QUIEN HIZO EL CAMBIO Y EN QUE FECHA*/
CREATE TABLE log_table (
    user_id  VARCHAR2(20) NOT NULL,
    column1  DATE,
    CONSTRAINT log_table_pk PRIMARY KEY ( user_id,
                                          column1 ) ENABLE
);

/*CREAMOS EL TRIGGER QUE SE EJECUTARA DESPUES DE EJECUTAR UN UPDATE EN SALARY*/
CREATE OR REPLACE TRIGGER log_sal_change_trigg AFTER
    UPDATE OF salary ON employees
BEGIN
    INSERT INTO log_table VALUES (
        user,
        sysdate
    );

    dbms_output.put_line(user || sysdate);
END;
/*HACER EL UPDATE*/
UPDATE employees
SET
    salary = salary * 1.5
WHERE
    employee_id = 100;

/*ESTE TRIGGER SE EJECUTARIA ANTES DE QUE ALGUIEN ENTRE A LA BASE DE DATOS
Y GUARDARIA SUS DATOS EN UNA TABLA
OLD Y NEW SON VARIABLES DE SISTEMA QUE ALMACENAN LOS DATOS DEL REGISTRO QUE ESTAS
A PUNTO DE MODIFICAR*/
CREATE OR REPLACE TRIGGER logon_trigg AFTER LOGON ON DATABASE BEGIN
    INSERT INTO log_table (
        user_id,
        logon_date
    ) VALUES (
        user,
        sysdate
    );

END;

/*ESTE TRIGGER VERIFICARA QUE CUANDO A UN EMPLEADO LE QUIERAS CAMBIAR EL ID SE ASEGURE QUE ESE EMPLEADO NO HAYA TENIDO
ANTERIORMETE ESE EMPLEO, HACIENDO USO DE OLD Y NEW, SI EN EL SELECT ENCUENTRA ALGO ENTONCES ESO QUIERE DECIR QUE SI HA TENIDO
EL EMPLEOO ANTES ENTONCES LANZA LA EXCEPCION*/
CREATE OR REPLACE TRIGGER check_sal_trigg BEFORE
    UPDATE OF job_id ON employees
    FOR EACH ROW
DECLARE
    v_job_count INTEGER;
BEGIN
    SELECT
        COUNT(*)
    INTO v_job_count
    FROM
        job_history
    WHERE
            employee_id = :old.employee_id
        AND job_id = :new.job_id;

    IF v_job_count > 0 THEN
        raise_application_error(-20201, 'This employee has already done this job');
    END IF;
END;



/*UN TRIGGER SE PUEDE EJECUTAR BEFORE, AFTER OR INSTEAD OF*/
/*SENTENCIA PARA DEFINER UN TRIGER
EL OBJECT NAME ES EL NOMBRE DE LA TABLA*/
CREATE [OR REPLACE] TRIGGER trigger_name
timing
event1 [OR event2 OR event3]ON object_name trigger_body


CREATE OR REPLACE TRIGGER sal_upd_trigg BEFORE
    UPDATE OF salary ON employees
BEGIN ...END;


CREATE OR REPLACE TRIGGER emp_del_trigg AFTER
    DELETE ON employees
BEGIN ...END;

CREATE OR REPLACE TRIGGER sal_upd_trigg
BEFORE UPDATE OF salary, commission_pct ON employees
BEGIN ... END;
/*PUEDE CONTENER UNAS DE UN EVENTO*/
CREATE OR REPLACE TRIGGER emp_del_trigg
AFTER INSERT OR DELETE OR UPDATE ON employees
BEGIN ... END;

/*PUEDE HABER TRIGGERS A NIVEL DE STATMENT O REGISTRO
DONDE NO IMPORTA CUANTOS REGISTROS SE CAMBIAN, SOLO CUANDO TERMINE TODO */
CREATE OR REPLACE TRIGGER log_emp_changes AFTER
    UPDATE ON employees/*AQUI*/
BEGIN
INSERT
    INTO log_emp_table
( who, when ) VALUES (
    user,
    sysdate
);
END;


/*------------------------------------------------------*/
/*AQUI IGUAL SE DECLARA UN TRIGGER POR STATETMENT */
CREATE OR REPLACE TRIGGER log_dept_changes AFTER
    INSERT OR UPDATE OR DELETE ON departments
BEGIN
    INSERT INTO log_dept_table (
        which_user,
        when_done
    ) VALUES (
        user,
        sysdate
    );

END;
/*ESTE TRIGGER NOS PERMITE LANZAR UNA EXCEPCION CUANDO LA FECHA SEA SABADO O DOMINGO, YA QUE SE REQUEIERE QUE SOLO SE PUEDAN INSERTAR EMPLEADOS ENTRE SEMANA*/
CREATE OR REPLACE TRIGGER secure_emp BEFORE
    INSERT ON employees
BEGIN
    IF to_char(sysdate, 'DY') IN ( 'SAT', 'SUN' ) THEN
        raise_application_error(-20500, 'You may insert into EMPLOYEES table only during
business hours');
    END IF;
END;


/*UN TRIGGER NO PUEDE TENER COMMITS O ROLLBACKS DENTRO */
CREATE OR REPLACE TRIGGER log_dept_changes AFTER
    INSERT OR UPDATE OR DELETE ON departments
BEGIN
    INSERT INTO log_dept_table (
        which_user,
        when_done
    ) VALUES (
        user,
        sysdate
    );

    COMMIT;
END;
/*TRIGGER CON CONDICION*/
CREATE OR REPLACE TRIGGER secure_emp BEFORE
    INSERT OR UPDATE OR DELETE ON employees
BEGIN
    IF to_char(sysdate, 'DY') IN ( 'SAT', 'SUN' ) THEN
        IF deleting THEN
            raise_application_error(-20501, 'You may delete from EMPLOYEES table only during business
hours');
        ELSIF inserting THEN
            raise_application_error(-20502, 'You may insert into EMPLOYEES table only during business
hours');
        ELSIF updating THEN
            raise_application_error(-20503, 'You may update EMPLOYEES table only during business hours');
        END IF;

    END IF;
END;
/*trigger con condicion*/
CREATE OR REPLACE TRIGGER secure_emp BEFORE
    UPDATE ON employees
BEGIN
    IF updating('SALARY') THEN
        IF to_char(sysdate, 'DY') IN ( 'SAT', 'SUN' ) THEN
            raise_application_error(-20501, 'You may not update SALARY on the weekend');
        END IF;

    ELSIF updating('JOB_ID') THEN
        IF to_char(sysdate, 'DY') = 'SUN' THEN
            raise_application_error(-20502, 'You may not update JOB_ID on Sunday');
        END IF;
    END IF;
END;


/*******--------------------------------------**************************/
/**********************TRIGGERS POR FILA********************************/
/*******--------------------------------------**************************/

/*SE UTILIZA "FOR EACH ROW"*/
CREATE OR REPLACE TRIGGER log_emps AFTER
    UPDATE OF salary ON employees
    FOR EACH ROW
BEGIN
INSERT
    INTO log_emp_table
( who, when ) VALUES (
    user,
    sysdate
);
END;

/*EJEMPLO*/
CREATE OR REPLACE TRIGGER log_emps AFTER
    UPDATE OF salary ON employees
    FOR EACH ROW
BEGIN
INSERT
    INTO log_emp_table
( who, when,which_employee,old_salary,new_salary ) VALUES (
    user,
    sysdate,:old.employee_id,:old.salary,:new.salary
);
END;

/*EJEMPLO*/
CREATE OR REPLACE TRIGGER audit_emp_values AFTER
    DELETE OR INSERT OR UPDATE ON employees
    FOR EACH ROW
BEGIN
    INSERT INTO audit_emp (
        user_name,
        time_stamp,
        id,
        old_last_name,
        new_last_name,
        old_title,
        new_title,
        old_salary,
        new_salary
    ) VALUES (
        user,
        sysdate,
        :old.employee_id,
        :old.last_name,
        :new.last_name,
        :old.job_id,
        :new.job_id,
        :old.salary,
        :new.salary
    );

END;

/*EJECUCION DEL CODIGO*/
INSERT
    INTO employees
(employee_id,
last_name,
job_id,
salary, ...)
VALUES (999, 'Temp emp', 'SA_REP', 1000,...);


UPDATE employees
SET
    salary = 2000,
    last_name = 'Smith'
WHERE
    employee_id = 999;
    
    
SELECT user_name, time_stamp, ...
FROM audit_emp;


/*Suppose you need to prevent employees who are not a
President or Vice-President from having a salary of
more than $15,000.*/

CREATE OR REPLACE TRIGGER restrict_salary BEFORE
    INSERT OR UPDATE OF salary ON employees
    FOR EACH ROW
BEGIN
    IF
        NOT ( :new.job_id IN ( 'AD_PRES', 'AD_VP' ) )
        AND :new.salary > 15000
    THEN
        raise_application_error(-20202, 'Employee cannot earn more than $15,000.');
    END IF;
END;

/*AQUI SE LANZA LA EXCEPCION YA QUE "DAVIES" ES UN EMPLEADO Y COMO LO HACE POR REGISTRO EL TRIGGER POR ESO SALE ERROR
PERO TAMPOCO ACTUALIZARA EL VALOR DE KING YA QUE EN UN TRIGGER POR EACH ROW SE DEBEN DE CUMPLIR TODAS LAS SENTENCIAS */
UPDATE employees
SET
    salary = 15500
WHERE
    last_name IN ( 'King', 'Davies' );
    
/*-------------------------------------------*/
/*AQUI EL TRIGGER SE ASEGURA QUE EL DEPARTAMENT_ID EXISTA, SI NO LO INSERTA ANTES DE HACER LA ACTUALIZACION*/
CREATE OR REPLACE TRIGGER employee_dept_fk_trg BEFORE
    UPDATE OF department_id ON employees
    FOR EACH ROW
DECLARE
    v_dept_id departments.department_id%TYPE;
BEGIN
    SELECT
        department_id
    INTO v_dept_id
    FROM
        departments
    WHERE
        department_id = :new.department_id;

EXCEPTION
    WHEN no_data_found THEN
        INSERT INTO departments VALUES (
            :new.department_id,
            'Dept ' || :new.department_id,
            NULL,
            NULL
        );

END;


UPDATE employees
SET
    department_id = 999
WHERE
    employee_id = 124;
    
    
/*-------------------------------------------------------*/
/*SI POR CASUALIDAD UNA TABLA SE LLAMA OLD, PODEMOS USAR CUALIFICADORES PARA NOMBRES A LA FUNCION "OLD Y NEW" DE ORACLE USANDO "REFERENING"*/  
CREATE OR REPLACE TRIGGER log_emps AFTER
    UPDATE OF salary ON old
    REFERENCING
            OLD AS former
            NEW AS latter
    FOR EACH ROW
BEGIN
INSERT
    INTO log_emp_table
( who, when,
which_employee,
old_salary,
new_salary ) VALUES (
    user,
    sysdate,
    :former.employee_id,
    :former.salary,
    :latter.salary
);
END;

/*-------------------------------------------------------*/
/*ESTE COIGO SE ASEGURA QUE EL SALARIO QUE SE AUMENTA SEA MAYOR AL QUE YA SE TENIA*/
CREATE OR REPLACE TRIGGER restrict_salary AFTER
    UPDATE OF salary ON employees
    FOR EACH ROW
BEGIN IF :new.salary > :old.salary THEN
INSERT
    INTO log_emp_table
( who,when,
which_employee,
old_salary,
new_salary ) VALUES (
    user,
    sysdate,
    :old.employee_id,
    :old.salary,
    :new.salary
);
END IF;END;
/*-------------------------------------------------------*/
/*SE PUEDE UTILIZAR WHEN EN LUGAR DEL IF*/
CREATE OR REPLACE TRIGGER restrict_salary AFTER
    UPDATE OF salary ON copy_employees
    FOR EACH ROW
    WHEN ( new.salary > old.salary )
BEGIN
INSERT
    INTO log_emp_table
( who,when,
which_employee,
old_salary,
new_salary ) VALUES (
    user,
    sysdate,
    :old.employee_id,
    :old.salary,
    :new.salary
);
END;
/*-------------------------------------------------------*/
/*INSTED OF SIEMPRE TRABAJA A NIVEL DE REGISTRO*/

CREATE TABLE new_emps
    AS
        SELECT
            employee_id,
            last_name,
            salary,
            department_id
        FROM
            employees;

CREATE TABLE new_depts
    AS
        SELECT
            d.department_id,
            d.department_name,
            SUM(e.salary) dept_sal
        FROM
            employees    e,
            departments  d
        WHERE
            e.department_id = d.department_id
        GROUP BY
            d.department_id,
            d.department_name;

CREATE VIEW emp_details AS
    SELECT
        e.employee_id,
        e.last_name,
        e.salary,
        e.department_id,
        d.department_name
    FROM
        new_emps   e,
        new_depts  d
    WHERE
        e.department_id = d.department_id;
        

/*NO ES RECOMENDABLE HACER LOS CAMBIOS EN LA VISTA, ASI QUE USAMOS INSTEAD OF PARA QUE HAGA LOS CAMBIOS EN LAS TABLAS EN LUGAR DE LA VISTA*/
CREATE OR REPLACE TRIGGER new_emp_dept INSTEAD OF
    INSERT ON emp_details
BEGIN
    INSERT INTO new_emps VALUES (
        :new.employee_id,
        :new.last_name,
        :new.salary,
        :new.department_id
    );

    UPDATE new_depts
    SET
        dept_sal = dept_sal + :new.salary
    WHERE
        department_id = :new.department_id;

END;


/*ESTE GUARDA LOS DATOS DE ALGUIEN QUE CREO UNA TABLA*/
CREATE OR REPLACE TRIGGER log_create_trigg AFTER CREATE ON SCHEMA BEGIN
    INSERT INTO log_table VALUES (
        user,
        sysdate
    );

END;

/*ESTE NO PERMITE QUE ALGUIEN BORRE UNA TABLA*/
CREATE OR REPLACE TRIGGER prevent_drop_trigg BEFORE DROP ON SCHEMA BEGIN
    raise_application_error(-20203, 'Attempted drop � failed');
END;

/*ESTE GUARDA LOS DATOS CUANDO ALGUIEN ENTRA Y CUANDO SALE*/
CREATE OR REPLACE TRIGGER logon_trig AFTER LOGON ON SCHEMA BEGIN
    INSERT INTO log_trig_table (
        user_id,
        log_date,
        action
    ) VALUES (
        user,
        sysdate,
        'Logging on'
    );

END;

CREATE OR REPLACE TRIGGER logoff_trig BEFORE LOGOFF ON SCHEMA BEGIN
    INSERT INTO log_trig_table (
        user_id,
        log_date,
        action
    ) VALUES (
        user,
        sysdate,
        'Logging off'
    );

END;

/*PODEMOS GUARDAR TODOS LOS ERRORES QUE SE GEERAN EN EL ESQUEMA CON AYUDA DE UN TRIGGER*/
CREATE OR REPLACE TRIGGER servererror_trig AFTER SERVERERROR ON SCHEMA
BEGIN IF ( is_servererror(942) ) THEN
INSERT
    INTO error_log_table
... END IF;
END;
/*CUANDO EL CUERPO DEL TRIGGER ES MUY COMPLICADO,S E PUDE ALMACENAR EN UN PROCEDIMIENTO Y DESPUES LLAMARLO, CUANDO SE HACE ESTO YA NO DSE PONE EL "END" AL FINAL*/
CREATE [OR REPLACE] TRIGGER trigger_name
timing
event1 [OR event2 OR event3]
ON table_name
[REFERENCING OLD AS old | NEW AS new]
[FOR EACH ROW]
[WHEN condition]
CALL procedure_name

CREATE OR REPLACE TRIGGER log_employee
BEFORE INSERT ON EMPLOYEES
CALL log_execution

/**You need to create a report showing the total salary bill
for a department. */

/*� First, we add a new column to the DEPARTMENTS table
to store the total salary bill for each department:*/
ALTER TABLE DEPARTMENTS
ADD total_salary NUMBER(12,2);

/*Populate this column with the current total dept salary:*/
UPDATE departments d
SET total_salary =
(SELECT SUM(salary) FROM employees
WHERE department_id = d.department_id);

/*A DML row trigger will keep this new column up to date
when salaries are changed.*/

CREATE OR REPLACE PROCEDURE increment_salary
(p_id IN NUMBER, p_new_sal IN NUMBER) IS
BEGIN
UPDATE copy_departments
SET total_salary = total_salary + NVL(p_new_sal,0)
WHERE department_id = p_id;
END increment_salary;

CREATE OR REPLACE TRIGGER compute_salary
AFTER INSERT OR UPDATE OF salary OR DELETE
ON employees FOR EACH ROW
BEGIN
IF DELETING THEN increment_salary
(:OLD.department_id,(:OLD.salary * -1));
ELSIF UPDATING THEN increment_salary
(:NEW.department_id,(:NEW.salary - :OLD.salary));
ELSE increment_salary
(:NEW.department_id,:NEW.salary);
END IF;
END;