/****************************************************************/
/*********************CURSORES Y FOR LOOPS CON PARAMETROS********/
/****************************************************************/

DECLARE
    CURSOR cur_country (p_region_id NUMBER) IS
    SELECT
        country_id,
        country_name
    FROM
        countries
    WHERE
        region_id = p_region_id;

    v_country_record cur_country%rowtype;
BEGIN
    OPEN cur_country(2);
    LOOP
        FETCH cur_country INTO v_country_record;
        EXIT WHEN cur_country%notfound;
        dbms_output.put_line(v_country_record.country_id
                             || ' '
                             || v_country_record.country_name);
    END LOOP;

    CLOSE cur_country;
END;


/*-------------------------------------------------------------*/
DECLARE
    v_deptid   employees.department_id%TYPE;
    CURSOR cur_emps (p_deptid NUMBER) IS
    SELECT
        employee_id,
        salary
    FROM
        employees
    WHERE
        department_id = p_deptid;
    v_emp_rec  cur_emps%rowtype;
BEGIN
    SELECT MAX(department_id) INTO v_deptid
    FROM
        employees;
    OPEN cur_emps(v_deptid);
    LOOP
        FETCH cur_emps INTO v_emp_rec;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_rec.employee_id
                             || ' '
                             || v_emp_rec.salary);
    END LOOP;

    CLOSE cur_emps;
    
END;

/*-------------------------------------------------------------*/
/*TAMBIEN POODEMOS USAR MULTIPLES PARAMETROS CON FOR LOOPS*/
DECLARE
    CURSOR cur_countries (
        p_region_id   NUMBER,
        p_population  NUMBER
    ) IS
    SELECT
        country_id,
        country_name,
        population
    FROM
        countries
    WHERE
        region_id = p_region_id
        OR population > p_population;

BEGIN
    FOR v_country_record IN cur_countries(145, 10000000) LOOP
        dbms_output.put_line(v_country_record.country_id
                             || ' '
                             || v_country_record.country_name
                             || ' '
                             || v_country_record.population);
    END LOOP;
END;

/*-----------------------------------------------------------*/
DECLARE
    CURSOR cur_emps (
        p_job     VARCHAR2,
        p_salary  NUMBER
    ) IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
            job_id = p_job
        AND salary > p_salary;

BEGIN
    FOR v_emp_record IN cur_emps('IT_PROG', 10000) LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;
/*-----------------------------------------------------------*/
