/*------------------------------------------------------------------*/
/*------------------------FUNCIONES---------------------------------*/
/*------------------------------------------------------------------*/

/*ESTRUCTURA*/
CREATE [OR REPLACE] FUNCTION function_name
[(parameter1 [mode1] datatype1, ...)]
RETURN datatype IS|AS
[local_variable_declarations; �]
BEGIN
-- actions;
RETURN expression;
END[
    function_name
];

/*------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION get_sal (
    p_id IN employees.employee_id%TYPE
) RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT
        salary
    INTO v_sal
    FROM
        employees
    WHERE
        employee_id = p_id;

    RETURN v_sal;
END get_sal;

/*------------------------------------------------------------------*/
/*ESTA ES IGUAL PERO TIENE UNA EXCEPCION*/
CREATE OR REPLACE FUNCTION get_sal (
    p_id IN employees.employee_id%TYPE
) RETURN NUMBER IS
    v_sal employees.salary%TYPE := 0;
BEGIN
    SELECT
        salary
    INTO v_sal
    FROM
        employees
    WHERE
        employee_id = p_id;

    RETURN v_sal;
EXCEPTION
    WHEN no_data_found THEN
    dbms_output.put_line('NOT FOUND Salary: ' ||(p_id));
        RETURN NULL;
END get_sal;

/*SE PUEDE INVOCAR UNA FUNCION CON UN BLOQUE ANINIMO Y CON UN SELECT*/
DECLARE v_sal employees.salary%type;
BEGIN
v_sal := get_sal(999); 
 dbms_output.put_line('Salary: ' ||(v_sal));
END;
/*--------------------*/
SELECT job_id, get_sal(employee_id) FROM employees;
/*------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION valid_dept (p_dept_no departments.department_id%TYPE
) RETURN BOOLEAN IS
    v_valid VARCHAR2(1);
BEGIN SELECT
    'x' INTO v_valid
FROM
    departments
WHERE
    department_id =
        p_dept_no;
    return(true);
EXCEPTION
    WHEN no_data_found THEN
        return(false);
    WHEN OTHERS THEN
        NULL;
END;
/*LLAMANDO LA FUNCION */
BEGIN
    IF valid_dept(1000) THEN
        dbms_output.put_line('TRUE');
    ELSE
        dbms_output.put_line('FALSE');
        DBMS_OUTPUT.PUT_LINE(USER);
    END IF;
END;

/*------------------------------------------------------------------*/
/*LLAMAR FUNCIONES SIN PARAMETROS*/
DBMS_OUTPUT.PUT_LINE(USER);

SELECT job_id, SYSDATE-hire_date FROM employees;

/*CREAMOS UNA FUNCION QUE CALCULA LOS IMPUESTOS POR CADA SALARIO*/
CREATE OR REPLACE FUNCTION tax ( p_value IN NUMBER) RETURN NUMBER IS
BEGIN
    return(p_value * 0.08);
END tax;
/*LLAMAMOS LA FUNCION EN UN SELECT*/
SELECT
    employee_id,
    last_name,
    salary,
    tax(salary)
FROM
    employees
WHERE
    department_id = 50;
/*ESTE OBTIENE EL IPMUESTO MASXIMO DEL DPTO 20 Y LUEGO REGRESA EL ID DE EMPLEADO Y LOS 
IMPUESTOS QUE PAGA SOLO CUANDO SUS IMPUESTOS SEAN MAYORES AL QUE CALCULO PRIMERO*/
SELECT
    employee_id,
    tax(salary)
FROM
    employees
WHERE
    tax(salary) > (
        SELECT
            MAX(tax(salary))
        FROM
            employees
        WHERE
            department_id = 20
    )
ORDER BY
    tax(salary) DESC;
/*------------------------------------------------------------------*/
select max(salary), employee_id from employees
where department_id = 80
