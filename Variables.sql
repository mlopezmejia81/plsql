/***********************************************************/
/*************************VARIABLES*************************/
/***********************************************************/DECLARE
    v_counter INTEGER := 0;
BEGIN
    v_counter := v_counter + 1;
    dbms_output.put_line(v_counter);
END;


/*TIPOS DE VARIABLES*/ DECLARE
    fam_birthdate   DATE;
    fam_size        NUMBER(2) NOT NULL := 10;
    fam_location    VARCHAR2(13) := 'Florida';
    fam_bank        CONSTANT NUMBER := 50000;
    fam_population  INTEGER;
    fam_name        VARCHAR2(20) DEFAULT 'Roberts';
    fam_party_size  CONSTANT PLS_INTEGER := 20;


/*MAS DECLARACION DE VARIABLES*/
                    declare         v_emp_hiredate date;
    v_emp_deptno    NUMBER(2) NOT NULL := 10;
    v_location      VARCHAR2(13) := 'Atlanta';
    c_comm          CONSTANT NUMBER := 1400;
    v_population    INTEGER;
    v_book_type     VARCHAR2(20) DEFAULT 'fiction';
    v_artist_name   VARCHAR2(50);
    v_firstname     VARCHAR2(20) := 'Rajiv';
    v_lastname      VARCHAR2(20) DEFAULT 'Kumar';
    c_display_no    constant
    pls_integer := 20;
/*---------------------------------------------------------*/
declare
    v_myname VARCHAR2(20);
BEGIN
    dbms_output.put_line('My name is: ' || v_myname);
    v_myname := 'John';
    dbms_output.put_line('My name is: ' || v_myname);
END;
/*--------------------------------------------*/
DECLARE
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate)
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;


/*------------------------------------------*/
/*Recibe un String y regresa el numero de las letras del STRING*/
CREATE FUNCTION num_characters (
    p_string IN VARCHAR2
) RETURN INTEGER IS
    v_num_characters INTEGER;
BEGIN
    SELECT
        length(p_string)
    INTO v_num_characters
    FROM
        dual;

    RETURN v_num_characters;
END;
/*Llamamos a la funcion y le pasamos su parametro*/
DECLARE
    v_length_of_string INTEGER;
BEGIN
    v_length_of_string := num_characters('Oracle
Corporationn');
    dbms_output.put_line(v_length_of_string);
END;



