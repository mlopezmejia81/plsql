/****************************************************************/
/*********************CURSORES PARA UPDATES**********************/
/****************************************************************/

/*� NOWAIT returns an Oracle server error immediately*/
/*- WAIT n waits for n seconds, and returns an Oracle server error
if the other session is still locking the rows at the end of that
time.*/
/*-WHERE CURRENT OF Use cursors to update or delete the current row.*/

/*FOR UPDATE OF" BLOQUEA UNA TABLA EN DONDE SE ENCUENTRE LA COLUMNA QUE LE DECIMOS 
FOR UPDATE BLOQUE LA TABLA */
/*ESTO SE USA CUANDO TENEMOS UN JOIN DE 2 TABLAS*/
DECLARE
    CURSOR emp_cursor IS
    SELECT
        e.employee_id,
        d.department_name
    FROM
        employees    e,
        departments  d
    WHERE
            e.department_id = d.department_id
        AND department_id = 80
    FOR UPDATE OF salary;
/*-----------------------------------------------------------------*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        salary
    FROM
        employees
    WHERE
        salary <= 20000
    FOR UPDATE NOWAIT;/*BLOQUEA LA TABLA , Y SI ALGUIEN ESTA USANDO LA TABLA NO QUIERO ESPERAR Y MUESTRAME EL ERROR DE 
    QUE ALGUIEN LA ESTA USANDO*/

    v_emp_rec cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_rec;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_rec.salary);
        UPDATE employees /*ACTUALIZA LA TABLA*/
        SET salary = v_emp_rec.salary * 1.1 /*INCREMENTA EL SALARIO EN 10%*/
         /*dbms_output.put_line(employees.salary);   */
        WHERE
            CURRENT OF cur_emps;/*DE DONDE?, DEL REGISTRO ACTUAL DONDE ESTAS*/
        
    END LOOP;

    CLOSE cur_emps;
END;



