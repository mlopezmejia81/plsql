

/*SI EN EL PROCEDIMIENTO MANEJAS LA EXCEPCION Y DONDE LA LLAMAS SIGUE EJECUTANDOLA
ENTONCES SE LANZARA LA EXCEPCION PERO
EL PROGRAMA SEGUIRA TRABAJANDO*/
CREATE OR REPLACE PROCEDURE add_department (
    p_name  VARCHAR2,
    p_mgr   NUMBER,
    p_loc   NUMBER
) IS
BEGIN
    INSERT INTO departments (
        department_id,
        department_name,
        manager_id,
        location_id
    ) VALUES (
        departments_seq.NEXTVAL,
        p_name,
        p_mgr,
        p_loc
    );

    dbms_output.put_line('Added Dept: ' || p_name);
EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('Error adding dept: ' || p_name);
end;
/*----*/
BEGIN
    add_department('Media', 100, 1800);
    add_department('Editing', 99, 1800);
    add_department('Advertising', 101, 1800);
END;


/*-AQUI NO SE MANEJA LA EXCPECION, ENTONCES SALE ERROR INMEDIATAMENTE*/
CREATE OR REPLACE PROCEDURE add_department_noex (
    p_name  VARCHAR2,
    p_mgr   NUMBER,
    p_loc   NUMBER
) IS
BEGIN
    INSERT INTO departments (
        department_id,
        department_name,
        manager_id,
        location_id
    ) VALUES (
        departments_seq.NEXTVAL,
        p_name,
        p_mgr,
        p_loc
    );

    dbms_output.put_line('Added Dept: ' || p_name);
END;

BEGIN
    add_department_noex('Media', 100, 1800);
    add_department_noex('Editing', 99, 1800);
    add_department_noex('Advertising', 101, 1800);
END;
