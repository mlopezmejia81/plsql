/***********************************************************/
/*************************BLOQUES ANIDADOS*************************/
/***********************************************************/
/*LOS BLOQUES EXTERNOS NO PUEDEN LEER LAS VARIABLES DE LOS INTERNOS
EN ESTE CASO EL BLOQUE EXTERNO NO PODRIA LEER "v_child_name" YA QUE ES DE UN BLOQUE EXTERNO */
DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Apr-1972';
BEGIN
    DECLARE
        v_child_name VARCHAR2(20) := 'Mike';
    BEGIN
        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
    END;
    /*dbms_output.put_line('Child''s Name: ' || v_child_name);*/
    dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
END;

/*------------------------------------------------
AQUI SE DECLARAN LAS VARIABLES EN LE BLOQUE EXTERNO Y SE HACEN GLOBALES PARA NO TENER PROBLEMAS DE QUE NO HAYA ACCESO 
A LAS VARIABLES
*/
DECLARE
    v_first_name  VARCHAR2(20);
    v_last_name   VARCHAR2(20);
BEGIN
    BEGIN
        v_first_name := 'Carmen';
        v_last_name := 'Miranda';
        dbms_output.put_line(v_first_name
                             || ' '
                             || v_last_name);
    END;

    dbms_output.put_line(v_first_name
                         || ' '
                         || v_last_name);
END;

/**/
/*CON EL OUTER PODEMOS ACCEDER A UNA VARIABLE EXTERNA
YA QUE EN ESTE CASO ESTAN DECLARADAS DOS VARIABLES CON EL MISMO NOMBRE, POR LO CUAL 
AL USAR outer.v_date_of_birth ACCEDE A LA VARIABLE DEL BLOQUE EXTERNO
LO DE ABAJO ES LA SINTAXIS PARA DECLARAR LA ETIQUETA <<outer>>*/
<< outer >> DECLARE
    v_father_name    VARCHAR2(20) := 'Patrick';
    v_date_of_birth  DATE := '20-Apr-1972';
BEGIN
    << outer2 >> DECLARE
        v_child_name     VARCHAR2(20) := 'Mike';
        v_date_of_birth  DATE := '12-Dec-2002';
    BEGIN
        DECLARE
            v_date_of_birth DATE := '12-Dec-2010';
        BEGIN
            dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
            dbms_output.put_line('Date of Birth: ' || outer2.v_date_of_birth);
        END;

        dbms_output.put_line('Father''s Name: ' || v_father_name);
        dbms_output.put_line('Date of Birth: ' || outer.v_date_of_birth);
        dbms_output.put_line('Child''s Name: ' || v_child_name);
        dbms_output.put_line('Date of Birth: ' || v_date_of_birth);
    END;
END;

