/*------------------------------------------------------------------*/
/*------------------------DICCIONARIO DE DATOS---------------------------------*/
/*------------------------------------------------------------------*/

DESCRIBE ALL_TABLES
/*------------------------------------------------------------------*/
SELECT table_name, owner FROM ALL_TABLES;
/*------------------------------------------------------------------*/
SELECT object_type, object_name FROM USER_OBJECTS;
/*------------------------------------------------------------------*/
SELECT object_type, COUNT(*) FROM USER_OBJECTS
GROUP BY object_type;
/*------------------------------------------------------------------*/
SELECT COUNT(*) FROM DICT WHERE table_name LIKE 'USER%';
/*------------------------------------------------------------------*/
SELECT * FROM DICT WHERE table_name LIKE 'USER%IND%';

SELECT departmens, owner FROM ALL_TABLES;

/*------------------------------------------------------------------*/
SELECT
    object_name
FROM
    user_objects
WHERE
    object_type = 'FUNCTION'; /**-- use 'PROCEDURE' to
see procedures*/



/*---------------------------------------------------*/
/*AQUI LLAMAS AL CODIGO FUENTE DE UNA FUNCION*/
SELECT text
FROM USER_SOURCE
WHERE name = 'TAX'
ORDER BY line;