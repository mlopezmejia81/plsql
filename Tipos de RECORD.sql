/****************************************************************/
/*********************Tipos de RECORD****************************/
/****************************************************************/

DECLARE
    TYPE person_dept IS RECORD (
        first_name       employees.first_name%TYPE,
        last_name        employees.last_name%TYPE,
        department_name  departments.department_name%TYPE
    );
    v_person_dept_rec person_dept;
BEGIN
    SELECT
        e.first_name,
        e.last_name,
        d.department_name
    INTO v_person_dept_rec
    FROM
             employees e
        JOIN departments d ON e.department_id = d.department_id
    WHERE
        employee_id = 200;

    dbms_output.put_line(v_person_dept_rec.first_name
                         || ' '
                         || v_person_dept_rec.last_name
                         || ' is in the '
                         || v_person_dept_rec.department_name
                         || ' department.');

END;



/*---------------------------------------------------------------*/
/*TYPE DENTRO DE UN TYPE*/
DECLARE
    TYPE dept_info_type IS RECORD (
        department_id    departments.department_id%TYPE,
        department_name  departments.department_name%TYPE
    );
    TYPE emp_dept_type IS RECORD (
        first_name  employees.first_name%TYPE,
        last_name   employees.last_name%TYPE,
        dept_info   dept_info_type
    );
    v_emp_dept_rec emp_dept_type;
BEGIN
...END;


/**-----------------------------------------------------*/
/*BLOQUE DENTRO DE UN BLOQUE*/

DECLARE -- outer block
    TYPE employee_type IS RECORD (
        first_name employees.first_name%TYPE := 'Amy'
    );
    v_emp_rec_outer employee_type;
BEGIN
    dbms_output.put_line(v_emp_rec_outer.first_name);
    DECLARE -- inner block
        v_emp_rec_inner employee_type;
    BEGIN
        v_emp_rec_outer.first_name := 'Clara';
        dbms_output.put_line(v_emp_rec_outer.first_name
                             || ' and '
                             || v_emp_rec_inner.first_name);
    END;

    dbms_output.put_line(v_emp_rec_outer.first_name);
END;