/****************************************************************/
/*********************Indexing Tables of Records****************************/
/****************************************************************/

/*� An INDEX BY table, which is based on a single field or column;
for example, the last_name column of the EMPLOYEES table.
� An INDEX BY table of records, which is based on a composite
record type; for example, the row structure in the
DEPARTMENTS table.*/

TYPE type_name IS TABLE OF DATA_TYPE 
INDEX BY PRIMARY_KEY_DATA_TYPE;
identifier type_name;

TYPE t_hire_date IS TABLE OF DATE
INDEX BY BINARY_INTEGER;
v_hire_date_tab t_hire_date;


/*ESTRUCTURA*/
DECLARE
    TYPE type_name IS
        TABLE OF data_type INDEX BY primary_key_data_type;
    identifier type_name;
BEGIN FOR record IN ( SELECT
    column
FROM TABLE ) LOOP
    identifier(primary_key) := record.column;
END LOOP;
END;
/* ... --------------------------------*/
DECLARE
    TYPE t_hire_date IS TABLE OF employees.hire_date% TYPE INDEX BY BINARY_INTEGER;
    v_hire_date_tab t_hire_date;
BEGIN
    FOR emp_rec IN (
        SELECT
            employee_id,
            hire_date
        FROM
            employees
    ) LOOP
        v_hire_date_tab(emp_rec.employee_id) := emp_rec.hire_date;
    END LOOP;
END;

/*---------------------------------------------*/
DECLARE
    TYPE t_hire_date IS
        TABLE OF employees.hire_date% TYPE INDEX BY BINARY_INTEGER;
    v_hire_date_tab  t_hire_date;
    v_count          BINARY_INTEGER := 0;
BEGIN
    FOR emp_rec IN (
        SELECT
            hire_date
        FROM
            employees
    ) LOOP
        v_count := v_count + 1;
        v_hire_date_tab(v_count) := emp_rec.hire_date;
    END LOOP;
END;
/*---------------------------------------------*/

DECLARE
    TYPE t_hire_date IS
        TABLE OF employees.hire_date%TYPE INDEX BY BINARY_INTEGER;
    v_hire_date_tab    t_hire_date;
    v_hire_date_count  NUMBER(4);
BEGIN
    FOR emp_rec IN (
        SELECT
            employee_id,
            hire_date
        FROM
            employees
    ) LOOP
        v_hire_date_tab(emp_rec.employee_id) := emp_rec.hire_date;
    END LOOP;

    dbms_output.put_line(v_hire_date_tab.count);
END;
/*---------------------------------------------*/
/*INDEX BY Table of Records*/
DECLARE
    TYPE t_emp_rec IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    v_emp_rec_tab t_emp_rec;
BEGIN
    FOR emp_rec IN (
        SELECT
            *
        FROM
            employees
    ) LOOP
        v_emp_rec_tab(emp_rec.employee_id) := emp_rec;
        dbms_output.put_line(v_emp_rec_tab(emp_rec.employee_id).salary);
    END LOOP;
END;