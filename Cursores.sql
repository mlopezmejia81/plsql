/****************************************************************/
/*****************************CURSORES***************************/
/****************************************************************/
/*CURSOR APUNTA A UN CONJUNTO DE REGISTROS*/
/*DESPUES EN EL BEGIN LO QUE HACE ES QUE ESOS REGISTROS LOS GUARDA 
EN LAS VARIABLES*/
DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name
    FROM
        departments
        ORDER BY department_name;

    v_department_id    departments.department_id%TYPE;
    v_department_name  departments.department_name%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name;
        EXIT WHEN cur_depts%notfound;
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name);
    END LOOP;

    CLOSE cur_depts;
END;



/*****************------------------------------********************/
DECLARE
    CURSOR cur_depts_emps IS
    SELECT
        department_name,
        COUNT(*) AS how_many
    FROM
        departments  d,
        employees    e
    WHERE
        d.department_id = e.department_id
    GROUP BY
        d.department_name
    HAVING
        COUNT(*) > 1;
        
/*------------------------------------**********************------------*/
declare cursor cur_depts IS SELECT
                                department_id,
                                department_name
                            FROM
                                departments
                                    v_department_id
    departments.department_id%TYPE;
    v_department_name  departments.department_name%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name;
        EXIT WHEN cur_depts%notfound;
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name);
    END LOOP;

    CLOSE cur_depts;
END;

/*****************------------------------------********************/
/*CUANDO SE TIENEN MUCHAS COLUMNAS QUE COPIAR LA MEJOR OPCION ES DECLARAR UNA VARIABLE
RECORD CON EL TIPO DEL CURSOR, DE ESTA FORMA ESTE "RECORD CONTENDRA TODAS LAS COLUMNAS DEL CURSOR
Y SUS TIPOS DE DATO". CUANDO INSERTEMOS LOS DATOS EN EL INTO, EL PROGRAMA INSERTARA AUTOMATICAMETNE TODOS LOS CAMPOS
*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        *
    FROM
        employees
    WHERE
        department_id = 30;

    v_emp_record cur_emps%rowtype;/*AQUI*/
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' - '
                             || v_emp_record.last_name
                             || ' - '
                             || v_emp_record.email
                             || ' - '
                             || v_emp_record.phone_number);
    END LOOP;

    CLOSE cur_emps;
END;

/*****************------------------------------********************/

DECLARE
    CURSOR cur_emps_dept IS
    SELECT
        e.first_name,
        e.last_name,
        d.department_name
    FROM
        employees    e,
        departments  d
    WHERE
        e.department_id = d.department_id;

    v_emp_dept_record cur_emps_dept%rowtype;
    /*v_number_rows NUM(20,2);*/
BEGIN
    OPEN cur_emps_dept;
    LOOP
        FETCH cur_emps_dept INTO v_emp_dept_record;
        EXIT WHEN cur_emps_dept%notfound;
        dbms_output.put_line(v_emp_dept_record.first_name
                             || ' � '
                             || v_emp_dept_record.last_name
                             || ' � '
                             || v_emp_dept_record.department_name
                             || ' � '
                              || cur_emps_dept%ROWCOUNT);
        
    END LOOP;
/*v_number_rows =cur_emps_dept%ROWCOUNT;*/
    CLOSE cur_emps_dept;
END;


/*****************------------------------------********************/

DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees;

    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
        FETCH cur_emps INTO v_emp_record;
        EXIT WHEN cur_emps%rowcount > 10 OR cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name
                             || ' � '
                             || cur_emps%ROWCOUNT);
    END LOOP;

    CLOSE cur_emps;
END;


/*****************------------------------------********************/
/*NO PODEMOS USAR UN ATRIBUTO DEL CURSO DIRECTAMENTE, TENEMOS QUE GUARDARLO EN UNA VARIABLE 
Y DESPUES YA HACERLO*/
declare cursor cur_emps is...;
    v_emp_record emp_cursor%rowtype;
    v_count NUMBER;
    v_rowcount NUMBER; -- declare variable to hold cursor attribute
BEGIN open cur_emps;loop
fetch cur_emps into v_emp_record;

EXIT WHEN cur_emps%notfound;

v_rowcount := cur_emps%rowcount; -- "copy" cursor attribute to variable
INSERT INTO top_paid_emps (
    employee_id,
    rank,
    salary
) VALUES (
    v_emp_record.employee_id,
    v_rowcount,
    v_emp_record.salary
); -- use
-- variable in SQL statement
