/*CODIGO PARA GENERAR COPIA DE TABLAS*/
BEGIN
    FOR c IN (
        SELECT
            *
        FROM
            user_tables
    ) LOOP
        EXECUTE IMMEDIATE 'CREATE TABLE Luis2.'
                          || c.table_name
                          || ' AS SELECT * FROM HR.'
                          || c.table_name;
    END LOOP;
END;

/*-**************************************************************/

BEGIN
    dbms_output.put_line('PL/SQL is easy!');
END;
/*----------------------------*/
DECLARE
    v_date DATE := sysdate + 1;
BEGIN
    dbms_output.put_line(v_date);
END;
/*------------------------------------------------------*/
DECLARE
    v_first_name  VARCHAR2(25);
    v_last_name   VARCHAR2(25);
BEGIN
    SELECT
        first_name,
        last_name
    INTO
        v_first_name,
        v_last_name
    FROM
        employees
    WHERE
        last_name = 'Hunold';
       /* last_name = 'King';*/
                    dbms_output.put_line('The employee of the month is: '
                         || v_first_name
                         || ' '
                         || v_last_name
                         || '.');

EXCEPTION /*Excepcion para cuando el SELECT regrese mas de una columna con el nombre Oswald*/
                    WHEN too_many_rows THEN
        dbms_output.put_line('Your select statement retrieved
multiple rows. Consider using a cursor or changing
the search criteria.');
END;
/*--------------------------------------------------------------------------*/

/***********************************************************/
/*************************PROCEDIMIENTOS********************/
/***********************************************************/
CREATE OR REPLACE PROCEDURE print_date IS
    v_date VARCHAR2(30);
BEGIN
    SELECT
        to_char(sysdate, 'Mon DD, YYYY')
    INTO v_date
    FROM
        dual;

    dbms_output.put_line(v_date);
END;

BEGIN
    print_date;
END;


/*-----------------------------------------------------------------------------*/
/*FUNCTION*/
/*Crear una funcio llamada "tomorrow"
con un parametro de entrada (por eso el "IN")  "p_today" de tipo DATE
y va a regresar lo que esta en la variable "v_tomorrow" de tipo DATE
despues al parametro de entrada le suma 1
lo insterta en v_tomorrow
de una tabla dummy
y regresa la variable v_tomorrow
*/
CREATE OR REPLACE FUNCTION tomorrow (
    p_today IN DATE
) RETURN DATE IS
    v_tomorrow DATE;
BEGIN
    SELECT
        p_today + 1
    INTO v_tomorrow
    FROM
        dual;

    RETURN v_tomorrow;
END;

/*Lo de arriba solo fue la funcion, hay que ejecutarla en otra sentencia
Primero lo podemos hacer con un SELECT 
seleccionamos la funcion "tomorrow" y le pasamos el parametro de entrada "sysdate", que es 
la fecha del servidor y le da un alias
*/
SELECT
    tomorrow(sysdate) AS "Tomorrow's Date"
FROM
    dual;
/*Esta es la otra forma de hacerlo con UN BLOQUE DE CODIGO
En esta sentencia  imprimimos la funcion con su parametro de entrada*/
or

BEGIN
    dbms_output.put_line(tomorrow(sysdate));
END;


