/****************************************************************/
/*************************Excepciones****************************/
/****************************************************************/
/*no_data_found*/
DECLARE
    v_country_name  countries.country_name%TYPE := 'Korea, South';
    v_reg_id        countries.region_id%TYPE;
BEGIN
    SELECT region_id INTO v_reg_id
    FROM countries
    WHERE country_name = v_country_name;
    DBMS_OUTPUT.PUT_LINE(v_country_name); 
EXCEPTION
    WHEN no_data_found THEN --Si el registro no tiene datos
                dbms_output.put_line('Country name, '
                             || v_country_name
                             || ',
        cannot be found. Re-enter the country name using the correct
        spelling.');
END;


/*-------------------------------------------------------------------------*/
/*TOO_MANY_ROWS*/
DECLARE
v_lname employees.last_name%TYPE;
BEGIN
SELECT last_name INTO v_lname
FROM employees WHERE job_id = 'ST_CLERK';
DBMS_OUTPUT.PUT_LINE('The last name of the ST_CLERK is: ' || v_lname);
EXCEPTION
WHEN TOO_MANY_ROWS THEN
DBMS_OUTPUT.PUT_LINE ('Your select statement retrieved multiple rows.
Consider using a cursor.');
END;


/*MULTIPLES EXCEPCIONES*/
BEGIN
...
...
EXCEPTION
WHEN NO_DATA_FOUND THEN
statement1;
statement2;
...
WHEN TOO_MANY_ROWS THEN
statement3;
statement4;
...
WHEN OTHERS THEN
statement5;
statement6;
...
END;
/*-------------------------------------------------------------------------*/
/*Excepciones predefinidas*/
 NO_DATA_FOUND
 TOO_MANY_ROWS
 INVALID_CURSOR
 ZERO_DIVIDE
 DUP_VAL_ON_INDEX

/*---------------------------------------------------------------------------*/
/*/*MULTIPLES EXCEPCIONES*/*/
DECLARE
    v_lname VARCHAR2(15);
BEGIN
    SELECT
        last_name
    INTO v_lname
    FROM
        employees
    WHERE
        job_id = 'ST_CLERK';

    dbms_output.put_line('The last name of the ST_CLERK is: ' || v_lname);
EXCEPTION
    WHEN too_many_rows THEN
        dbms_output.put_line('Select statement found multiple rows');
    WHEN no_data_found THEN
        dbms_output.put_line('Select statement found no rows');
    WHEN OTHERS THEN
        dbms_output.put_line('Another type of error occurred');
END;


/*DEFINIENDO EXCEPCIONES*/
DECLARE
e_insert_excep EXCEPTION;
PRAGMA EXCEPTION_INIT(e_insert_excep, -01400);/*el -01400 el el codigo opara definir la excepcion*/
BEGIN
INSERT INTO departments
(department_id, department_name)
VALUES (280, NULL);
EXCEPTION
WHEN e_insert_excep /*Aqui se llama a la excepcion*/
THEN
DBMS_OUTPUT.PUT_LINE('INSERT FAILED');
END;

/*------------------------------------------------------------------------------------*/
/*GUARDAR EN UNA TABLA LOS ERRORES QUE SE GENERAN*/
DECLARE
v_error_code NUMBER;
v_error_message VARCHAR2(255);
BEGIN ...
EXCEPTION
WHEN OTHERS THEN
ROLLBACK;
v_error_code := SQLCODE; /*SQLCODE returns the numeric value for the error code. (You
can assign it to a NUMBER variable.)*/
v_error_message := SQLERRM;/* SQLERRM returns character data containing the message
associated with the error number. */
INSERT INTO error_log(e_user, e_date, error_code, error_message)
VALUES(USER, SYSDATE, v_error_code, v_error_message);
END;


/*-----------------------------------------------*/
/*CAPTURAS EXCEPCIONES DE LOS USUARIOS*/
DECLARE
    e_invalid_department EXCEPTION;/*DEFINIR EXCEPCION*/
    v_name    VARCHAR2(20) := 'Accounting';
    v_deptno  NUMBER := 27;
BEGIN
    UPDATE departments
    SET
        department_name = v_name
    WHERE
        department_id = v_deptno;

    IF SQL%notfound THEN   /*SI NO LO ENCUENTRA ENTONCES LANZA LA EXCEPCION*/
        RAISE e_invalid_department;
    END IF;
EXCEPTION /*ATRAPAR LA EXCEPCION*/
    WHEN e_invalid_department THEN
        dbms_output.put_line('No such department id.');
END;
/*-----------------------------------------------*/
/***********************************************************************************/
/*********************LANZANDO EXCEPCIONES******************************************/
/***********************************************************************************/
/*RAISE_APPLICATION_ERROR
LE ASIGNAS UN NUMERO A LA EXCEPCION Y LO LANZAS CON RAISE_APPLICATION_ERROR*/
DECLARE
    v_mgr PLS_INTEGER := 123;
BEGIN
    DELETE FROM employees
    WHERE
        manager_id = v_mgr;

    IF SQL%notfound THEN
        raise_application_error(-20202, 'This is not a valid manager');
    END IF;
END;
/*-----------------------------------------------*/

DECLARE
    v_mgr          PLS_INTEGER := 102;/*27,100*/
    v_employee_id  employees.employee_id%TYPE;
BEGIN
    SELECT
        employee_id
    INTO v_employee_id
    FROM
        employees
    WHERE
        manager_id = v_mgr;

    dbms_output.put_line('Employee #'
                         || v_employee_id
                         || ' works for manager #'
                         || v_mgr
                         || '.');

EXCEPTION
    WHEN no_data_found THEN
        raise_application_error(-20201, 'This manager has no employees');
    WHEN too_many_rows THEN
        raise_application_error(-20202, 'Too many employees were found.');
END;
/*-----------------------------------------------*/
DECLARE
    e_name EXCEPTION;
    PRAGMA exception_init ( e_name, -20999 );
    v_last_name employees.last_name%TYPE := 'Silly Name';
BEGIN
    DELETE FROM employees
    WHERE
        last_name = v_last_name;

    IF SQL%rowcount = 0 THEN
        raise_application_error(-20999, 'Invalid last name');
    ELSE
        dbms_output.put_line(v_last_name || ' deleted');
    END IF;

EXCEPTION
    WHEN e_name THEN
        dbms_output.put_line('Valid last names are: ');
        FOR c1 IN (
            SELECT DISTINCT
                last_name
            FROM
                employees
        ) LOOP
            dbms_output.put_line(c1.last_name);
        END LOOP;

    WHEN OTHERS THEN
        dbms_output.put_line('Error deleting from employees');
END;


/*--------------------------------------------------------*/
/*AQUI DECLARAMOS UNA EXCEPCION  CON UN CODIGO
SI SE CUMPLE QUE LA CUENTA DE ROW ES 0, LO QUE SIGNIFICA QUE NO SE EJECUTO EL CODIGO, 
ENTONCES SE LANZA LA EXCEPCION -20999 Y LA ATRAPAMOS EN LA PARTE DE ABAJO,
DESPUES MOSTRAMOS LOS NOMBRES QUE SI PUEDEN SER ELIMINADOS DENTRO DE LA EXCEPCION*/
DECLARE
    e_name EXCEPTION;
    PRAGMA exception_init ( e_name, -20999 );
    v_last_name employees.last_name%TYPE := 'Pepe';
BEGIN
    DELETE FROM employees
    WHERE
        last_name = v_last_name;

    IF SQL%rowcount = 0 THEN
        raise_application_error(-20999, 'Invalid last name');
    ELSE
        dbms_output.put_line(v_last_name || ' deleted');
    END IF;

EXCEPTION
    WHEN e_name THEN
        dbms_output.put_line('Valid last names are: ');
        FOR c1 IN (
            SELECT DISTINCT
                last_name
            FROM
                employees
        ) LOOP
            dbms_output.put_line(c1.last_name);
        END LOOP;

    WHEN OTHERS THEN
        dbms_output.put_line('Error deleting from employees');
END;
/***********************************************************************************/
/****************************ALCANCE DE LAS EXCEPCIONES*****************************/
/***********************************************************************************/

/*MANEJANDO EXCEPCIONES INTERNAMENTE*/
BEGIN -- outer block
...
BEGIN -- inner block
... -- exception_name occurs here
...
EXCEPTION
WHEN exception_name THEN -- handled here
...
END; -- inner block terminates successfully
... -- outer block continues execution
END;
/*MANEJANDO EXCEPCIONES EXTERNAMENTE*/
DECLARE -- outer block
e_no_rows EXCEPTION;
BEGIN
BEGIN -- inner block
IF ... THEN RAISE e_no_rows; - exception occurs here
...
END; -- Inner block terminates unsuccessfully
... -- Remaining code in outer blocks executable
... -- section is skipped
EXCEPTION
WHEN e_no_rows THEN  outer block handles the exception
...
END;

/****-----------------------------------------------*/
/*AQUI ES UN EJEMPLO DEL MANEJO INTERNO O EXTERNO DE LAS EXCEPCIONES
SI LA MANEJAMOS INTERNAMENTE ENTONCES SE IMPRIMIRIA EL MENSAJE 2 Y 3
PERO SI NO LA MANEJAMOS INTERNAMENTE ENTONCES SOLO SE MUESTRA EL MENSAJE 4
YA QUE SE ROMPE EL CODIGO Y SE INTERRUMPE
Y SE VA DIRECTO A BUSCAR UNA EXCEPCION*/
DECLARE
    v_last_name employees.last_name%TYPE;
BEGIN
    BEGIN
        SELECT
            last_name
        INTO v_last_name
        FROM
            employees
        WHERE
            employee_id = 999;

        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN no_data_found THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN OTHERS THEN
        dbms_output.put_line('Message 4');
END;

/****---------------------------*/
/*AQUI SALE ERROR YA QUE EL BLOQUE EXTERIOR NO TIENE ACCESO A 
LA ESXCEPCION QUE ESTA DECLARADA EN EL BLOQUE INTERIOR*/
BEGIN
    DECLARE
        e_myexcep EXCEPTION;
    BEGIN
        RAISE e_myexcep;
        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN too_many_rows THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN e_myexcep THEN
        dbms_output.put_line('Message 4');
END;
/*------------------------------------------------------*/
/*AQUI YA LA DECLARAMOS EN EL BLOQUE EXTERIOR Y NO HAY ERROR, SI ATRAPA LA EXCEPCION*/
DECLARE
    e_myexcep EXCEPTION;
BEGIN
    BEGIN
        RAISE e_myexcep;
        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN too_many_rows THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN e_myexcep THEN
        dbms_output.put_line('Message 4');
END;
/*------------------------------------------------------*/
DECLARE
    e_myexcep EXCEPTION;
BEGIN
    BEGIN
        RAISE e_myexcep;
        dbms_output.put_line('Message 1');
    EXCEPTION
        WHEN too_many_rows THEN
            dbms_output.put_line('Message 2');
    END;

    dbms_output.put_line('Message 3');
EXCEPTION
    WHEN no_data_found THEN
        dbms_output.put_line('Message 4');
END;
