/***********************************************************/
/******************RECUPERACION DE DATOS********************/
/***********************************************************/

DECLARE
    v_emp_hiredate  employees.hire_date%TYPE;
    v_emp_salary    employees.salary%TYPE;
BEGIN
    SELECT
        hire_date,
        salary
    INTO
        v_emp_hiredate,
        v_emp_salary
    FROM
        employees
    WHERE
        employee_id = 100;

    dbms_output.put_line('Hiredate: ' || v_emp_hiredate);
    dbms_output.put_line('Salary: ' || v_emp_salary);
END;

DECLARE
    v_sum_sal  NUMBER(10, 2);
    v_deptno   NUMBER NOT NULL := 60;
BEGIN
    SELECT
        SUM(salary) -- group function
    INTO v_sum_sal
    FROM
        employees
    WHERE
        department_id = v_deptno;

    dbms_output.put_line('Dep #60 Salary Total: ' || v_sum_sal);
END;




/*-------------------------------------------------------*/
BEGIN
    INSERT INTO copy_emp (
        employee_id,
        first_name,
        last_name,
        email,
        hire_date,
        job_id,
        salary
    ) VALUES (
        99,
        'Ruth',
        'Cores',
        'RCORES',
        sysdate,
        'AD_ASST',
        4000
    );

END;

/*---------------------------------------*/
DECLARE
    v_sal_increase employees.salary%TYPE := 800;
BEGIN
    UPDATE copy_emp
    SET
        salary = salary + v_sal_increase
    WHERE
        job_id = 'ST_CLERK';

END;

/*---------------------------------------*/
DECLARE
    v_deptno employees.department_id%TYPE := 10;
BEGIN
    DELETE FROM copy_emp
    WHERE
        department_id = v_deptno;

END;

/*--------------------------------------------------*/
BEGIN MERGE INTO copy_emp c
USING employees e ON ( e.employee_id = c.employee_id )
WHEN MATCHED THEN UPDATE
SET c.first_name = e.first_name,
    c.last_name = e.last_name,
    c.email = e.email,
. . .
WHEN NOT MATCHED THEN
INSERT VALUES(e.employee_id, e.first_name,...e.department_id );
END;

/*-------------------------------*/
DECLARE
    v_deptno employees.department_id%TYPE := 10;
BEGIN
    DELETE FROM copy_emp
    WHERE
        department_id = v_deptno;

END;

/*----------------------------------------------------------*/
/*SQL%ROWCOUNT
Nos regresa el numero de registros que fueron borrados*/
DECLARE
    v_deptno copy_employees.department_id%TYPE := 20;
BEGIN
    DELETE FROM copy_employees
    WHERE
        department_id = v_deptno;

    dbms_output.put_line(SQL%rowcount || ' rows deleted.');
END;

/*------------------------------------------------------*/
DECLARE
    v_sal_increase employees.salary%TYPE := 800;
BEGIN
    UPDATE copy_emp
    SET
        salary = salary + v_sal_increase
    WHERE
        job_id = 'ST_CLERK';

    dbms_output.put_line(SQL%rowcount || ' rows updated.');
END;

/*---------------------------------------*/
/*CREAMOS UNA TABLA CON UN CAMPO num_results*/
CREATE TABLE results (
    num_rows NUMBER(4)
);

/*---------------------*/
/*aumenta el salario de alguien y despues lo inserta en la tabla que se creo antes*/
BEGIN
    UPDATE copy_emp
    SET
        salary = salary + 100
    WHERE
        job_id = 'ST_CLERK';

    INSERT INTO results ( num_rows ) VALUES ( SQL%rowcount );

END;


/*------------------------------------------------*/
/*Hace lo mismo que la de arriba pero antes de insterat el valor en la tabla prmero lo guarda
en una variable llamada v_rowcount*/
DECLARE
    v_rowcount INTEGER;
BEGIN
    UPDATE copy_emp
    SET
        salary = salary + 100
    WHERE
        job_id = 'ST_CLERK';

    dbms_output.put_line(SQL%rowcount || ' rows in COPY_EMPupdated.');
    v_rowcount := SQL%rowcount;
    INSERT INTO results ( num_rows ) VALUES ( v_rowcount );

    dbms_output.put_line(SQL%rowcount || ' rows in RESULTS updated.');
END;



/*------------------------------------------------------------*/

/*SAVEPOINT*/
BEGIN
    INSERT INTO pairtable VALUES (
        7,
        8
    );

    SAVEPOINT my_sp_1;
    INSERT INTO pairtable VALUES (
        9,
        10
    );

    SAVEPOINT my_sp_2;
    INSERT INTO pairtable VALUES (
        11,
        12
    );

    ROLLBACK TO my_sp_1;
    INSERT INTO pairtable VALUES (
        13,
        14
    );

    COMMIT;
END;

