/***********************************************************/
/*************************TIPOS DE DATOS*************************/
/***********************************************************/
/*DATO COMPUESTO*/
/*guarda en una varibale que va a tomar el tipo de dato del registro de una tabla de empleados*/
/**/

v_emp_record employees%rowtype;
v_emp_record.first_name


/*La explicacion
Cuando no sabemos bien que tipo definirle a una variable
lo podemos hacer de la siguiente forma*/
/*Esta si estamos seguro del tipo y tama;o de la variable*/
v_first_name VARCHAR2(20);
/*Esta es la otra forma donde le decimos que sea del tipo last_name*/
v_first_name employees.last_name%TYPE;


DECLARE
    v_first_name employees.first_name%TYPE;/*Este es el ejemplo*/
BEGIN
    SELECT
        first_name
    INTO v_first_name
    FROM
        employees
    WHERE
        last_name = 'Vargas';

    dbms_output.put_line(v_first_name);
END;

/*------------------------------------------*/
/*Se pueden asignar tipos de variables de acuerdo a otras variables*/
DECLARE
    v_first_name   employees.first_name%TYPE;
    v_salary       employess.salary%TYPE;
    v_old_salary   v_salary%TYPE;
    v_new_salary   v_salary%TYPE;
    v_balance      NUMBER(10, 2);
    v_min_balance  v_balance%TYPE := 1000;








/*--------------------------------------*/
DECLARE
    v_date1  DATE := '05-Apr-2015';
    v_date2  DATE := v_date1 + 7;
    v_date3  TIMESTAMP := sysdate;
    v_date4  TIMESTAMP WITH TIME ZONE := sysdate;
BEGIN
    dbms_output.put_line(v_date1);
    dbms_output.put_line(v_date2);
    dbms_output.put_line(v_date3);
    dbms_output.put_line(v_date4);
END;

/*-----------------------------------------*/
DECLARE
v_valid1 BOOLEAN := TRUE;
v_valid2 BOOLEAN; v_valid3 boolean NOT
null := false;

BEGIN
    IF v_valid1 THEN
        dbms_output.put_line('Test is TRUE');
    ELSE
        dbms_output.put_line('Test is FALSE');
    END IF;
END;


/*-------------------------------------*/
/*CONVERSION IMPLICITA (AUTOMATICA)*/
DECLARE
    v_salary        NUMBER(6) := 6000;
    v_sal_increase  VARCHAR2(5) := '1000';
    v_total_salary  v_salary%TYPE;
BEGIN
    v_total_salary := v_salary + v_sal_increase;
    dbms_output.put_line(v_total_salary);
END;


/*----------------------------------------------
CONVERSION EXPLICITA*/
DECLARE
    v_a  VARCHAR2(10) := '-123456';
    v_b  VARCHAR2(10) := '+987654';
    v_c  PLS_INTEGER;
BEGIN
    v_c := to_number(v_a) + to_number(v_b);
    dbms_output.put_line(v_c);
END;


