/*------------------------------------------------------------------*/
/*------------------------PROCEDIMIENTOS----------------------------*/
/*------------------------------------------------------------------*/
CREATE [OR REPLACE] PROCEDURE procedure_name
[(parameter1 [mode] datatype1,
parameter2 [mode] datatype2, ...)]
IS|AS
[local_variable_declarations; ]
BEGIN
-- actions;
END[
    procedure_name
];

/*------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE add_dept IS
    v_dept_id    departments.department_id%TYPE;
    v_dept_name  departments.department_name%TYPE;
BEGIN
    v_dept_id := 280;
    v_dept_name := 'ST-Curriculum';
    INSERT INTO departments (
        department_id,
        department_name
    ) VALUES (
        v_dept_id,
        v_dept_name
    );

    dbms_output.put_line('Inserted '
                         || SQL%rowcount
                         || ' row.');
END;



/*Ejecutando el procedimiento*/
BEGIN
    add_dept;
END;

SELECT
    department_id,
    department_name
FROM
    departments
WHERE
    department_id = 280;
/*------------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE mainproc...
IS
PROCEDURE subproc (...) IS BEGIN
...
END subproc;
BEGIN
...
subproc(...);
...END mainproc;

/*-------------------------------------------------------------*/
CREATE OR REPLACE PROCEDURE delete_emp (
    p_emp_id IN employees.employee_id%TYPE
)
IS PROCEDURE log_emp (
    p_emp IN employees.employee_id%TYPE
) IS BEGIN
INSERT
    INTO logging_table VALUES ( p_emp,...);END
log_emp;

BEGIN
    DELETE FROM employees
    WHERE
        employee_id = p_emp_id;

    log_emp(p_emp_id);
END delete_emp;


/*----------------------------------------------------------------------*/
/*PASANDO PARAMETRO DE ENTRADA (IN)*/
/*CREAMOS UN PROCDIMIENTO QUE TENDRA 2 PARAMETROS DE ENTRADA
EL CUAL AUMENTRARA EL SALARIO DE UN EMPLEADO
*/

CREATE OR REPLACE PROCEDURE raise_salary (
    p_id       IN employees.employee_id%TYPE,
    p_percent  IN  NUMBER
) IS
BEGIN
    UPDATE employees
    SET
        salary = salary * ( 1 + p_percent / 100 )
    WHERE
        employee_id = p_id;

END raise_salary;
/*AQUI LO MANDAMOS LLAMAR*/
BEGIN
    raise_salary(176, 10);
END;
/*AQUI LO LLAMAMOS DENTRO DE OTRO PROCEDIMIENTO*/
CREATE OR REPLACE PROCEDURE process_employees IS
    CURSOR emp_cursor IS
    SELECT
        employee_id
    FROM
        employees;

BEGIN
    FOR v_emp_rec IN emp_cursor LOOP
        raise_salary(v_emp_rec.employee_id, 10);
    END LOOP;
END process_employees;

/*----------------------------------------------------*/
/**********PARAMETROS DE SALIDA Y ENTRADA**************/
CREATE OR REPLACE PROCEDURE query_emp (
    p_id      IN   employees.employee_id%TYPE,
    p_name    OUT  employees.last_name%TYPE,
    p_salary  OUT  employees.salary%TYPE
) IS
BEGIN
    SELECT
        last_name,
        salary
    INTO
        p_name,
        p_salary
    FROM
        employees
    WHERE
        employee_id = p_id;

END query_emp;
/*---------------*/
DECLARE
    a_emp_name  employees.last_name%TYPE;
    a_emp_sal   employees.salary%TYPE;
BEGIN
    query_emp(178, a_emp_name, a_emp_sal);
    dbms_output.put_line('Name: ' || a_emp_name);
    dbms_output.put_line('Salary: ' || a_emp_sal);
END;

/*----------------------------------------------------*/
CREATE OR REPLACE PROCEDURE format_phone (
    p_phone_no IN OUT VARCHAR2
) IS
BEGIN
    p_phone_no := '('
                  || substr(p_phone_no, 1, 3)
                  || ')'
                  || substr(p_phone_no, 4, 3)
                  || '-'
                  || substr(p_phone_no, 7);
END format_phone;

DECLARE
    a_phone_no VARCHAR2(13);
BEGIN
    a_phone_no := '8006330575';
    format_phone(a_phone_no);
    dbms_output.put_line('The formatted number is: ' || a_phone_no);
END;

/*----------------------------------------------------------*/

/*--------------------------------------*/
CREATE OR REPLACE PROCEDURE add_dept (
    p_name  IN  my_depts.department_name%TYPE,
    p_loc   IN  my_depts.location_id%TYPE
) IS
BEGIN
    INSERT INTO my_depts (
        department_id,
        department_name,
        location_id
    ) VALUES (
        departments_seq.NEXTVAL,
        p_name,
        p_loc
    );

END add_dept;


/*-----*/
/*FORMAS DE PSAR PARAMETROS*/
add_dept('EDUCATION', 1400);

add_dept(p_loc => 1400, p_name => 'EDUCATION');

/*--------------------------------------*/
CREATE OR REPLACE PROCEDURE show_emps (
    p_emp_id         IN  NUMBER,
    p_department_id  IN  NUMBER,
    p_hiredate       IN  DATE
)

add_dept ('EDUCATION', p_loc=>1400);

Positional notation :
show_emps (101, 10, 01-dec-2006)
 Named notation :
show_emps(p_department_id => 10,
p_hiredate => 01-dec-1007, p_emp_id => 101
 Combination notation :
show_emps(101, p_hiredate => 01-dec-2007,
p_department_id = 10)

/*------------------------------------------------------------------------------*/
/*CON VALORES DEFAULT*/
CREATE OR REPLACE PROCEDURE add_dept (
    p_name  my_depts.department_name%TYPE := 'Unknown',
    p_loc   my_depts.location_id%TYPE DEFAULT 1400
) IS BEGIN
INSERT
    INTO my_depts
(...) VALUES (
    departments_seq.nextval,
    p_name,
    p_loc
);
END
add_dept;


/*FORMAS DE LLAMRLO*/
add_dept;
add_dept ('ADVERTISING', p_loc => 1400);
add_dept (p_loc => 1400);
/*------------------------------------------------------------------------------*/
