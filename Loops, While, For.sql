/***********************************************************************/
/*********************************LOOP**********************************/
/***********************************************************************/

DECLARE
    v_counter NUMBER(2) := 1;
BEGIN
    LOOP
        dbms_output.put_line('Loop execution #' || v_counter);
        v_counter := v_counter + 1;
        EXIT WHEN v_counter > 5;
    END LOOP;
END;

/*--------------------------------------------*/
DECLARE
    v_loc_id   locations.location_id%TYPE;
    v_counter  NUMBER(2) := 1;
BEGIN
    SELECT
        MAX(location_id)
    INTO v_loc_id
    FROM
        locations
    WHERE
        country_id = 2;

    LOOP
        INSERT INTO locations (
            location_id,
            city,
            country_id
        ) VALUES (
            ( v_loc_id + v_counter ),
            'Montreal',
            2
        );

        v_counter := v_counter + 1;
        EXIT WHEN v_counter > 3;
    END LOOP;

END;


/**-**************-------------------------*/
/*ROMPER UN LOOP*/
DECLARE
    v_counter NUMBER := 1;
BEGIN
    LOOP
        dbms_output.put_line('Counter is ' || v_counter);
        v_counter := v_counter + 1;
        IF v_counter > 10 THEN
            EXIT;
        END IF;
    END LOOP;
END;




/***********************************************************************/
/*********************************WHILE**********************************/
/***********************************************************************/
DECLARE
    v_loc_id   locations.location_id%TYPE;
    v_counter  NUMBER := 1;
BEGIN
    SELECT
        MAX(location_id)
    INTO v_loc_id
    FROM
        locations
    WHERE
        country_id = 2;

    WHILE v_counter <= 3 LOOP
        INSERT INTO locations (
            location_id,
            city,
            country_id
        ) VALUES (
            ( v_loc_id + v_counter ),
            'Montreal',
            2
        );

        v_counter := v_counter + 1;
    END LOOP;

END;

/*--------------------------------------------*/
/***********************************************************************/
/**********************************FOR**********************************/
/***********************************************************************/
DECLARE
    v_loc_id locations.location_id%TYPE;
BEGIN
    SELECT
        MAX(location_id)
    INTO v_loc_id
    FROM
        locations
    WHERE
        country_id = 2;

    FOR i IN 1..3 LOOP
        INSERT INTO locations (
            location_id,
            city,
            country_id
        ) VALUES (
            ( v_loc_id + i ),
            'Montreal',
            2
        );

    END LOOP;

END;


DECLARE
    v_lower  NUMBER := 1;
    v_upper  NUMBER := 100;
BEGIN FOR i IN v_lower..v_upper LOOP
...END loop;
END;


/*****************************************************************/
/***********************************************************************/
/**********************************NESTED LOOPS**********************************/
/***********************************************************************/


BEGIN
    FOR v_outerloop IN 1..3 LOOP
        FOR v_innerloop IN REVERSE 1..5 LOOP
            dbms_output.put_line('Outer loop is: '
                                 || v_outerloop
                                 || ' and inner loop is: '
                                 || v_innerloop);
        END LOOP;
    END LOOP;
END;
/*--------------------------------------------*/
/*LOOP LABELS*/
DECLARE
    v_outerloop  PLS_INTEGER := 0;
    v_innerloop  PLS_INTEGER := 5;
BEGIN
    << outer_loop >> LOOP
        v_outerloop := v_outerloop + 1;
        v_innerloop := 5;
        EXIT WHEN v_outerloop > 3;
        << inner_loop >> LOOP
            dbms_output.put_line('Outer loop is: '
                                 || v_outerloop
                                 || ' and inner loop is: '
                                 || v_innerloop);
            v_innerloop := v_innerloop - 1;
            EXIT WHEN v_innerloop = 0;
        END LOOP inner_loop;

    END LOOP outer_loop;
END;