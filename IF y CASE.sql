
/***********************************************************************/
/***********************************************************************/
/***********************************IF**********************************/
/***********************************************************************/


IF condition THEN
statements;
[ELSIF condition THEN
statements;]
[ELSE
statements;]
END IF;
/********-----------------------******************/
DECLARE
    v_myage NUMBER := 31;
BEGIN
    IF v_myage < 11 THEN
        dbms_output.put_line('I am a child');
    ELSIF v_myage < 20 THEN
        dbms_output.put_line('I am young');
    ELSIF v_myage < 30 THEN
        dbms_output.put_line('I am in my twenties');
    ELSIF v_myage < 40 THEN
        dbms_output.put_line('I am in my thirties');
    ELSE
        dbms_output.put_line('I am mature');
    END IF;
END;



/********-----------------------******************/
DECLARE
    v_myage NUMBER := 31;
BEGIN 
IF v_myage < 11 THEN
    dbms_output.put_line('I am a child');
ELSIF v_myage < 20 THEN
    dbms_output.put_line('I am young');
ELSIF v_myage < 30 THEN
    dbms_output.put_line('I am in my twenties');
ELSIF v_myage < 40 THEN
    dbms_output.put_line('I am in my thirties');
    ELSE
        dbms_output.put_line('I am mature');
end i

/********-----------------------******************/
/***********************************************************************/
/***********************************CASE**********************************/
/***********************************************************************/
DECLARE
    v_num  NUMBER := 15;
    v_txt  VARCHAR2(50);
BEGIN
    CASE v_num
        WHEN 20 THEN
            v_txt := 'number equals 20';
        WHEN 17 THEN
            v_txt := 'number equals 17';
        WHEN 15 THEN
            v_txt := 'number equals 15';
        WHEN 13 THEN
            v_txt := 'number equals 13';
        WHEN 10 THEN
            v_txt := 'number equals 10';
        ELSE
            v_txt := 'some other number';
    END CASE;

    dbms_output.put_line(v_txt);
END;

/*-------------------------------------------------*/
DECLARE
    v_num  NUMBER := 15;
    v_txt  VARCHAR2(50);
BEGIN
    CASE
        WHEN v_num > 20 THEN
            v_txt := 'greater than 20';
        WHEN v_num > 15 THEN
            v_txt := 'greater than 15';
        ELSE
            v_txt := 'less than 16';
    END CASE;

    dbms_output.put_line(v_txt);
END;


/**--------------------------------------------------------------*/
/*EL RESULTADO DE UN CASE SE LE ASIGNARA A UNA VARIABLE*/
DECLARE
    v_out_var  VARCHAR2(15);
    v_in_var   NUMBER;
BEGIN
...
v_out_var := CASE v_in_var
WHEN 1 THEN 'Low value'
WHEN 50 THEN 'Middle value'
WHEN 99 THEN 'High value'
ELSE 'Other value'
END;
...END;

/**--------------------------------------------------------------*/
DECLARE
    v_grade      CHAR(1) := 'A';
    v_appraisal  VARCHAR2(20);
BEGIN
    v_appraisal :=
        CASE v_grade
            WHEN 'A' THEN
                'Excellent'
            WHEN 'B' THEN
                'Very Good'
            WHEN 'C' THEN
                'Good'
            ELSE 'No such grade'
        END;

    dbms_output.put_line('Grade: '
                         || v_grade
                         || ' Appraisal: '
                         || v_appraisal);
END;
/**--------------------------------------------------------------*/
/*SE PUEDEN METER UNA VARIABLE ENTRE EL "WHEN" Y "THEN" */
DECLARE
    v_out_var  VARCHAR2(15);
    v_in_var   NUMBER := 20;
BEGIN
    v_out_var :=
        CASE v_in_var
            WHEN 1 THEN
                'Low value'
            WHEN v_in_var THEN
                'Same value'
            WHEN 20 THEN
                'Middle value'
            ELSE 'Other value'
        END;

    dbms_output.put_line(v_out_var);
END;

/**--------------------------------------------------------------*/
/*EVALLUAR SIN SELECTOR*/
DECLARE
    v_grade      CHAR(1) := 'A';
    v_appraisal  VARCHAR2(20);
BEGIN
    v_appraisal :=
        CASE -- no selector here
            WHEN v_grade = 'A' THEN
                'Excellent'
            WHEN v_grade IN ( 'B', 'C' ) THEN
                'Good'
            ELSE 'No such grade'
        END;

    dbms_output.put_line('Grade: '
                         || v_grade
                         || ' Appraisal '
                         || v_appraisal);
END;
/**--------------------------------------------------------------*/
/*EVALLUAR SIN SELECTOR Y SE LO ASIGNA A UNA VARIABLE*/
DECLARE
    v_grade      CHAR(1) := 'A';
    v_appraisal  VARCHAR2(20);
BEGIN
    v_appraisal :=
        CASE
            WHEN v_grade = 'A' THEN
                'Excellent'
            WHEN v_grade IN ( 'B', 'C' ) THEN
                'Good'
            ELSE 'No such grade'
        END;

    dbms_output.put_line('Grade: '
                         || v_grade
                         || ' Appraisal '
                         || v_appraisal);
END;