/*******************************************************************/
/*******************************PAQUETES****************************/
/*******************************************************************/
/*PARA CREAR UN PAQUETE PRIMERO SE TIENE QUE ESPECIFICAR CON SUS
PROCEDIMIENTOS, FUNCIONES Y VARIABLES DENTRO DE EL*/
CREATE OR REPLACE PACKAGE check_emp_pkg IS
    g_max_length_of_service CONSTANT NUMBER := 100;
    
    PROCEDURE chk_hiredate (
        p_date IN employees.hire_date%TYPE
    );

    PROCEDURE chk_dept_mgr (
        p_empid  IN  employees.employee_id%TYPE,
        p_mgr    IN  employees.manager_id%TYPE
    );

END check_emp_pkg;
/*EN EL PACKAGE BODY ESTA EL CODIGO EJECUTABLE QUE LLAMA A LOS SUBPROGRAMAS DE 
EL PAQUETE, PUEDE CONTENER SUS PROPIAS VARIABLES*/
/*package body */
CREATE OR REPLACE PACKAGE BODY check_emp_pkg IS

    PROCEDURE chk_hiredate (
        p_date IN employees.hire_date%TYPE
    ) IS
    BEGIN
        IF months_between(sysdate, p_date) > g_max_length_of_service * 12 THEN
            raise_application_error(-20200, 'Invalid Hiredate');
        END IF;
    END chk_hiredate;

    PROCEDURE chk_dept_mgr (
        p_empid  IN  employees.employee_id%TYPE,
        p_mgr    IN  employees.manager_id%TYPE
    ) IS
    BEGIN
        dbms_output.put_line('Id: ');
    END chk_dept_mgr;

END check_emp_pkg;
/*CAMBIANDO EL CODIGO DEL BODY*/
CREATE OR REPLACE PACKAGE BODY check_emp_pkg IS
    PROCEDURE chk_hiredate (
        p_date IN employees.hire_date%TYPE
    ) IS
    BEGIN
        IF months_between(sysdate, p_date) > g_max_length_of_service * 12 THEN
            raise_application_error(-20201, 'Hiredate Too Old');
        END IF;
    END chk_hiredate;

PROCEDURE chk_dept_mgr (
    p_empid  IN  employees.employee_id%TYPE,
    p_mgr    IN  employees.manager_id%TYPE
) IS BEGIN dbms_output.put_line('Id: ');
END chk_dept_mgr;
END check_emp_pkg;


/*SE PUEDE DECRIBIR UN PAQUETE COMO SI FUER AUN A TABLA*/
DESCRIBE check_emp_pkg

/*OTRO EJEMPLO DE DECLARAR UN PAQUETE*/
CREATE OR REPLACE PACKAGE manage_jobs_pkg IS
    g_todays_date DATE := sysdate;
    CURSOR jobs_curs IS
    SELECT
        employee_id,
        job_id
    FROM
        employees
    ORDER BY
        employee_id;

    PROCEDURE update_job (
        p_emp_id IN employees.employee_id%TYPE
    );

    PROCEDURE fetch_emps (
        p_job_id  IN   employees.job_id%TYPE,
        p_emp_id  OUT  employees.employee_id%TYPE
    );

END manage_jobs_pkg;

/*UNA VARIABLE, FUNCION, PROCESO SERA GLOBAL (PUBLICA) SI ESTA DECLARADA EN LA DEFINICION
Y SERA LOCAL (PRIVADA) SI ESTA DECLARADA DENTRO DEL BODY DEL PAQUETE*/
CREATE OR REPLACE PACKAGE salary_pkg IS
    g_max_sal_raise CONSTANT NUMBER := 0.20;
    PROCEDURE update_sal (
        p_employee_id  employees.employee_id%TYPE,
        p_new_salary   employees.salary%TYPE
    );

END salary_pkg;


CREATE OR REPLACE PACKAGE BODY salary_pkg IS

    FUNCTION validate_raise -- private function

     (
        p_old_salary  employees.salary%TYPE,
        p_new_salary  employees.salary%TYPE
    ) RETURN BOOLEAN IS
    BEGIN
        IF p_new_salary > ( p_old_salary * ( 1 + g_max_sal_raise ) ) THEN
            RETURN false;
        ELSE
            RETURN true;
        END IF;
    END validate_raise;

    PROCEDURE update_sal -- public procedure

     (
        p_employee_id  employees.employee_id%TYPE,
        p_new_salary   employees.salary%TYPE
    ) IS
        v_old_salary employees.salary%TYPE; -- local variable
    BEGIN
        SELECT
            salary
        INTO v_old_salary
        FROM
            employees
        WHERE
            employee_id = p_employee_id;

        IF validate_raise(v_old_salary, p_new_salary) THEN
            UPDATE employees
            SET
                salary = p_new_salary
            WHERE
                employee_id = p_employee_id;

        ELSE
            raise_application_error(-20210, 'Raise too high');
        END IF;

    END update_sal;

END salary_pkg;

/*INVOCAR A LOS PAQUETES Y SUS SUBPROGRAMAS*/
DECLARE
v_bool BOOLEAN;
v_number NUMBER;
BEGIN
salary_pkg.update_sal(100,25000); -- 1
v_bool := salary_pkg.validate_raise(24000,25000);
/*update_sal(100,25000); -- 2
/*v_bool := salary_pkg.validate_raise(24000,25000); -- 3
v_number := salary_pkg.g_max_sal_raise; -- 4
v_number := salary_pkg.v_old_ salary; -- 5*/
END;


/**-------------------------------------------------*/
/*----------------SOBRECARGA DE PAQUETES------------*/
/**-------------------------------------------------*/
CREATE OR REPLACE PACKAGE emp_pkg IS
    PROCEDURE find_emp -- 1
     (
        p_employee_id  IN   NUMBER,
        p_last_name    OUT  VARCHAR2
    );

    PROCEDURE find_emp -- 2

     (
        p_job_id     IN   VARCHAR2,
        p_last_name  OUT  VARCHAR2
    );

    PROCEDURE find_emp -- 3

     (
        p_hiredate   IN   DATE,
        p_last_name  OUT  VARCHAR2
    );

END emp_pkg;

DECLARE
    v_last_name VARCHAR2(30);
BEGIN
    emp_pkg.find_emp('IT_PROG', v_last_name);
END;

/*PPARA QUE PUEDAS USAR UN PROCEDIMIENTO INVOCADO DENTRO DEL BODY PACKAGE
DEBES DE HACER LA DEFINICION PRIMERO Y PUEDES HACER LA DECLARACIONEN CUALQUIER PARTE DEL BODY,
PERO LA DEFINICION DEBE DE IR PRIMERO */
CREATE OR REPLACE PACKAGE BODY forward_pkg IS 
PROCEDURE calc_rating (...); -- forward declaration
-- Subprograms defined in alphabetical order
PROCEDURE award_bonus(...) IS
BEGIN
calc_rating (...); -- resolved by forward declaration
...
END;
PROCEDURE calc_rating (...) IS -- implementation
BEGIN
...END;END forward_pkg;
/**-------------------------------------------------------------------*/
/*SE PUEDEN DEFINIR BLOQUES ANONIMOS DENTRO DE EL BODY DE UN PAQUETE
PARA QUE HAGAN AUTOMATICAMENTE OPERACIONES CUANDO MANDEMOS LLAMAR EL PAQUETE
*/
CREATE OR REPLACE PACKAGE taxes_pkg IS
    g_tax NUMBER;
... -- declare all public procedures/functions
END taxes_pkg;
CREATE OR replace package body taxes_pkg is... -- declare all private variables
    ... -- define public/private procedures/functions
BEGIN -- unnamed initialization block
    SELECT
        rate_value
    INTO g_tax
    FROM
        tax_rates
    WHERE
        rate_name = 'TAX';

END taxes_pkg;

/*---------------------------------------------------------------------------*/
/*PODEMOS DECLARAR VARIABLES EN UNA DELCARACION DE PAQUETE Y PONERLAS DE MANERA GLOBAL PARA PODERLAS USAR EN  TODOS
LOS PAQUETES*/
CREATE OR REPLACE PACKAGE global_consts IS
    mile_to_kilo CONSTANT NUMBER := 1.6093;
    kilo_to_mile CONSTANT NUMBER := 0.6214;
    yard_to_meter CONSTANT NUMBER := 0.9144;
    meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;

GRANT EXECUTE ON global_consts TO PUBLIC;

/*AQUI LLAMAMOS A LAS CONSTANTES DECLARADAS EN EL PAQUETE DE CONSTANTES Y FUNCIONA CORRECTAMENTE*/
DECLARE
    distance_in_miles  NUMBER(5) := 5000;
    distance_in_kilo   NUMBER(6, 2);
BEGIN
    distance_in_kilo := distance_in_miles * global_consts.mile_to_kilo;
    dbms_output.put_line(distance_in_kilo);
END;

/*---------------------------------------------------------------*/
/*PODEMOS DECLARAR EXCEPCIONES DENTRO DE LA DECLARACION DEL PAQUETE*/
CREATE OR REPLACE PACKAGE our_exceptions IS
    e_cons_violation EXCEPTION;
    PRAGMA exception_init ( e_cons_violation, -2292 );
    e_value_too_large EXCEPTION;
    PRAGMA exception_init ( e_value_too_large, -1438 );
END our_exceptions;

GRANT EXECUTE ON our_exceptions TO PUBLIC;

/*AQUI USAMOS LAS EXCEPCIONES DECLARADAS EN EL PAQUETE DE ARRIBA*/
/*PRIMERO CREAMOS LA TABLA*/
CREATE TABLE excep_test (number_col NUMBER(3));
/*-*/
BEGIN
    INSERT INTO excep_test ( number_col ) VALUES ( 1000);

EXCEPTION
    WHEN our_exceptions.e_value_too_large THEN
        dbms_output.put_line('Value too big for column data
type');
END;

SELECT *
FROM excep_test


/*------------------------------------------------------*/
CREATE OR REPLACE PACKAGE taxes_pkg IS
    FUNCTION tax (
        p_value IN NUMBER
    ) RETURN NUMBER;

END taxes_pkg;

CREATE OR REPLACE PACKAGE BODY taxes_pkg IS

    FUNCTION tax (
        p_value IN NUMBER
    ) RETURN NUMBER IS
        v_rate NUMBER := 0.08;
    BEGIN
        return(p_value * v_rate);
    END tax;

END taxes_pkg;

SELECT
    taxes_pkg.tax(salary) "Impuestos",
    salary,
    last_name
FROM
    employees
/* ... ----------------------------*/

/*This example breaks the rule that a function
called from a query must not execute DML*/

CREATE OR REPLACE PACKAGE sal_pkg IS
FUNCTION sal (p_emp_id IN NUMBER) RETURN NUMBER;
END sal_pkg;

CREATE OR REPLACE PACKAGE BODY sal_pkg IS

    FUNCTION sal (
        p_emp_id IN NUMBER
    ) RETURN NUMBER IS
        v_sal employees.salary%TYPE;
    BEGIN
        UPDATE employees
        SET
            salary = salary * 2
        WHERE
            employee_id = p_emp_id;

        SELECT
            salary
        INTO v_sal
        FROM
            employees
        WHERE
            employee_id = p_emp_id;

        return(v_sal);
    END sal;

END sal_pkg;

SELECT
    sal_pkg.sal(100),
    salary,
    last_name
FROM
    employees;
    
/*--------------------------------------------------------*/
/*AQUI CREAMOS UN PROCEDIMIENTO EN DONDE TENEMOS UN PARAMETRO DE SALIDA QUE 
NOS DEVUELVE LA INFORMACION DE EMPLYEES DEPENDIENDO DEL ID
*/
CREATE OR REPLACE PROCEDURE sel_one_emp (
    p_emp_id  IN   employees.employee_id%TYPE,
    p_emprec  OUT  employees%rowtype
) IS BEGIN
    SELECT
        *
    INTO p_emprec
    FROM
        employees
    WHERE
        employee_id = p_emp_id;
EXCEPTION WHEN no_data_found THEN 
dbms_output.put_line('NOU');
END sel_one_emp;
/*AQUI "v_emprec" REGRESA LA INFORMACION*/
DECLARE
    v_emprec employees%rowtype;
BEGIN sel_one_emp(100, v_emprec);
dbms_output.put_line(v_emprec.last_name);
END;


/*-----------------------------------------------------*/
/*EL PROBELAMD E ESTE CODIGO ES QUE EL TIPO LO USAMOS PRIMERO Y LO DECLARAMOS DESPUES
Y NO DEBE DE SER ASI, PRIMERO SE DEBE DE DECLARAR EL TIPO Y DESPUES SE USAN*/
CREATE OR REPLACE PROCEDURE sel_emp_dept (
    p_emp_id        IN   employees.employee_id%TYPE,
    p_emp_dept_rec  OUT  ed_type
) IS

    TYPE ed_type IS RECORD (
        f_name  employees.first_name%TYPE,
        l_name  employees.last_name%TYPE,
        d_name  departments.department_name%TYPE
    );
BEGIN
    SELECT
        e.first_name,
        e.last_name,
        d.department_name
    INTO
        ed_type.f_name,
        ed_type.l_name,
        ed_type.d_name
    FROM
             employees e
        JOIN departments d USING ( department_id )
    WHERE
        employee_id = p_emp_id;

END sel_emp_dept;

/*--------------------------------------------------*/
CREATE OR REPLACE PACKAGE emp_dept_pkg IS
    TYPE ed_type IS RECORD (
        f_name  employees.first_name%TYPE,
        l_name  employees.last_name%TYPE,
        d_name  departments.department_name%TYPE
    );
    PROCEDURE sel_emp_dept (
        p_emp_id        IN   employees.employee_id%TYPE,
        p_emp_dept_rec  OUT  ed_type
    );

END emp_dept_pkg;
-- And create the package body as usual

DECLARE
    v_emp_dept_rec emp_dept_pkg.ed_type;
BEGIN
    emp_dept_pkg.sel_emp_dept(100, v_emp_dept_rec);
END;

/*------------------------------*/
/*CREAMOS UN PAQUETE CON UNA TABLA TIPO CLAVE VALOR DE INDEX BINARIO
/*USANDO INDICES EN PAQUETES */
CREATE OR REPLACE PACKAGE emp_pkg IS
TYPE emprec_type IS TABLE OF employees%ROWTYPE
INDEX BY BINARY_INTEGER;
PROCEDURE get_employees(p_emp_table OUT emprec_type);/*AQUI LE DICE QUE LA CALVE ES EL ID Y EL VALOR ES EL emp_record*/
END emp_pkg;


CREATE OR REPLACE PACKAGE BODY emp_pkg IS
PROCEDURE get_employees(p_emp_table OUT emprec_type) IS
BEGIN
FOR emp_record IN (SELECT * FROM employees)
LOOP
p_emp_table(emp_record.employee_id) := emp_record;
END LOOP;
END get_employees;
END emp_pkg;

DECLARE
v_emp_table emp_pkg.emprec_type;
BEGIN
emp_pkg.get_employees(v_emp_table);
FOR i IN v_emp_table.FIRST..v_emp_table.LAST
LOOP
IF v_emp_table.EXISTS(i) THEN
DBMS_OUTPUT.PUT_LINE(v_emp_table(i).employee_id || ' id ' );
END IF;
END LOOP;
END;
