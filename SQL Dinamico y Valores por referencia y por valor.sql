/****************************************************************************/
/*******************************Using Dynamic SQL****************************/
/****************************************************************************/

/*CUANDO QUEREMOS EJECUTAR ALGO PERO DEPENDE EN TIEMPO DE EJECUCUION DE UN PARAMETRO ENTONCES SALDRA UN ERROR
PARA SOLUCIONAR ESTO TENEMOS LA SENTENCIA EXECUTE*/

CREATE PROCEDURE drop_any_table (
    p_table_name VARCHAR2
) IS BEGIN
    drop TABLE p_table_name; -- cannot be parsed
END;
/*PARA ESO DEBEMOS DE DEFINIR UN EXECUTE INMEDIATE*/

CREATE PROCEDURE drop_any_table(p_table_name VARCHAR2) IS
BEGIN
EXECUTE IMMEDIATE 'DROP TABLE ' || p_table_name;
END;

CREATE PROCEDURE drop_any_table(p_table_name VARCHAR2) IS
v_dynamic_stmt VARCHAR2(50);
BEGIN
v_dynamic_stmt := 'DROP TABLE ' || p_table_name;
EXECUTE IMMEDIATE v_dynamic_stmt;
END;

BEGIN drop_any_table('EMPLOYEE_NAMES'); END;

CREATE FUNCTION del_rows (
    p_table_name VARCHAR2
) RETURN NUMBER IS
BEGIN
    EXECUTE IMMEDIATE 'DELETE FROM ' || p_table_name;
    RETURN SQL%rowcount;
END;
/*LLAMAMOS AQUI EL PROCEDIMIENTO*/
DECLARE
    v_count NUMBER;
BEGIN
    v_count := del_rows('EMPLOYEE_NAMES');
    dbms_output.put_line(v_count || ' rows deleted.');
END;

/*------------------------------------------------------------------------*/
/*TAMBIEN SE PUEDE EJECUTAR CADA PROCESO SI TENER QUE LLAMARLO MUCHAS VECES, CON UNA FUNCION QUE LOS 
EJECUTE TODOS
*/
ALTER PROCEDURE procedure-name COMPILE;
ALTER FUNCTION function-name COMPILE;
ALTER PACKAGE package_name COMPILE SPECIFICATION;
ALTER PACKAGE package-name COMPILE BODY;

CREATE PROCEDURE compile_plsql
(p_name VARCHAR2,p_type VARCHAR2,p_options VARCHAR2 := NULL) IS
v_stmt VARCHAR2(200);
BEGIN
v_stmt := 'ALTER ' || p_type || ' ' || p_name || ' COMPILE'
|| ' ' || p_options;
EXECUTE IMMEDIATE v_stmt;
END;
    
/*SI TIENES UNA BD QUE TIENE UN MILLON DE RGISTROS Y LOS GUARDAS EN UN LUGAR Y LUEGO LOS PASAS A OTRO LUGAR
ENTONCES SE TOMARIA MUCHO TIEMPO*/
BEGIN compile_plsql('MYPACK','PACKAGE','BODY'); END; 7
/*PASAR POR VALOR Y POR REFERENCIA*/
CREATE OR REPLACE PACKAGE emp_pkg IS
    TYPE t_emp IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    PROCEDURE emp_proc (
        p_small_arg  IN   NUMBER,
        p_big_arg    OUT  t_emp
    );
...END emp_pkg;

/*AQUI AL AGREGAR LA LINEA "OUT NO COPY", LO QUE HARA ES QUE PASARAN EL VALOR POR REFERENCIA
Y ASI NO SE COPIARAN LOS VALORES, SOLO APUTARA A ELLOS*/
CREATE OR REPLACE PACKAGE emp_pkg IS
    TYPE t_emp IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    PROCEDURE emp_proc (
        p_small_arg  IN NUMBER,
        p_big_arg    OUT NOCOPY t_emp
    );
...END emp_pkg;

/*DETERMINISTIC*/
/*SI A UNA FUNCION LE DAS EL MISMO VALOR DE ENTRADA TE DARA EL MISMO VALOR DE SALIDA
EN ESTE PRIMERO CODIGO NOS SALE ERROR PORQUE NO ESTA DECLARADO COMO DETERMINISTICO*/
CREATE OR REPLACE FUNCTION twicenum (
    p_number IN NUMBER
) RETURN NUMBER IS
BEGIN
    RETURN p_number * 2;
END twicenum;

/*CREAR UN INDICE*/
CREATE INDEX emp_twicesal_idx ON
    employees ( twicenum(salary) );
/*AQUI LE DECIMOS QUE SI ES DETERMINISTIC*/
CREATE OR REPLACE FUNCTION twicenum (
    p_number IN NUMBER
) RETURN NUMBER
    DETERMINISTIC
IS
BEGIN
    RETURN p_number * 2;
END twicenum;
/*CREAMOS INDICE*/
CREATE INDEX emp_twicesal_idx ON
    employees ( twicenum(salary) );
    
    
    
/*ESTE NO PUEDE SER DETERMINISTIC YA QUE EL SALARIOI DE LOS EMPLEADOS PODRIA CAMBIAR*/
CREATE OR REPLACE FUNCTION total_sal (
    p_dept_id IN employees.department_id%TYPE
) RETURN NUMBER
    DETERMINISTIC
IS
    v_total_sal NUMBER;
BEGIN
    SELECT
        SUM(salary)
    INTO v_total_sal
    FROM
        employees
    WHERE
        department_id = p_dept_id;

    RETURN v_total_sal;
END total_sal;

/*BULK COLLECT*/
/*SIRVE PARA SUSTITUIR UN CURSOR YA QUE ESTE NO COPIA CON EL FETCH LOS DATOS EN UN LUGAR SINO QUE VA POR TODOS ELLOS DE UN SOLO GOLPE
ES MEJOR PARA MEJORR EL DESEMPE�O DEL PROGRAMA*/
CREATE OR REPLACE PROCEDURE fetch_all_emps IS
/*AQUI SE DEFINE UN TIPO TIPO TABLA QUE TENDRA UN INDICE BINARIO ENTERO */
    TYPE t_emp IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    v_emptab t_emp;
BEGIN
/*VA POR TODOS LOS DATOS    */
    SELECT
        *
    BULK COLLECT
    INTO v_emptab
    FROM
        employees;

    FOR i IN v_emptab.first..v_emptab.last LOOP
        IF v_emptab.EXISTS(i) THEN
            dbms_output.put_line(v_emptab(i).last_name);
        END IF;
    END LOOP;

END fetch_all_emps;
/*AQUI VAMOS POR TODOS LOS SALARIOS DE LOS EMPLEADOS*/
CREATE OR REPLACE PROCEDURE fetch_some_emps IS
    TYPE t_salary IS
        TABLE OF employees.salary%TYPE INDEX BY BINARY_INTEGER;
    v_saltab t_salary;
BEGIN
    SELECT
        salary
    BULK COLLECT
    INTO v_saltab
    FROM
        employees
    WHERE
        department_id = 20
    ORDER BY
        salary;

    FOR i IN v_saltab.first..v_saltab.last LOOP
        IF v_saltab.EXISTS(i) THEN
            dbms_output.put_line(v_saltab(i));
        END IF;
    END LOOP;

END fetch_some_emps;

/*PARA MEJORAR EL FUNCIONAMIENTO DEL CODIGO SE USA UN FOR ALL*/
CREATE OR REPLACE PROCEDURE update_emps IS
    TYPE t_emp_id IS
        TABLE OF employees.employee_id%TYPE INDEX BY BINARY_INTEGER;
    v_emp_id_tab t_emp_id;
BEGIN
    SELECT
        employee_id
    BULK COLLECT
    INTO v_emp_id_tab
    FROM
        employees;

    FORALL i IN v_emp_id_tab.first..v_emp_id_tab.last
        UPDATE new_employees
        SET
            salary = salary * 1.05
        WHERE
            employee_id = v_emp_id_tab(i);

END update_emps;

/*FOR ALL CON UPDATE*/
CREATE OR REPLACE PROCEDURE update_emps IS
    TYPE t_emp_id IS
        TABLE OF employees.employee_id%TYPE INDEX BY BINARY_INTEGER;
    v_emp_id_tab t_emp_id;
BEGIN
    SELECT
        employee_id
    BULK COLLECT
    INTO v_emp_id_tab
    FROM
        employees;

    FORALL i IN v_emp_id_tab.first..v_emp_id_tab.last
        UPDATE new_employees
        SET
            salary = salary * 1.05
        WHERE
            employee_id = v_emp_id_tab(i);

END update_emps;


/*---------------------------------------------*/
CREATE OR REPLACE PROCEDURE insert_emps IS
    TYPE t_emps IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    v_emptab t_emps;
BEGIN
    SELECT
        *
    BULK COLLECT
    INTO v_emptab
    FROM
        employees;

    FORALL i IN v_emptab.first..v_emptab.last
        INSERT INTO emp VALUES v_emptab ( i );

    FOR i IN v_emptab.first..v_emptab.last LOOP
        dbms_output.put_line('Inserted: '
                             || i
                             || ' '
                             || SQL%bulk_rowcount(i)
                             || 'rows');
    END LOOP;

END insert_emps;


/*-------------------------------------------------*/
/*PODEMOS AGREGAR UNA EXCEPCION A EL FORR ALL PARA GUARDAR LLOS ERRORES */
CREATE OR REPLACE PROCEDURE insert_emps IS
TYPE t_emps IS TABLE OF employees%ROWTYPE INDEX BY BINARY_INTEGER;
v_emptab t_emps;
BEGIN
SELECT * BULK COLLECT INTO v_emptab FROM employees;
FORALL i IN v_emptab.FIRST..v_emptab.LAST SAVE EXCEPTIONS
INSERT INTO employees VALUES v_emptab(i);
END insert_emps;

/*Y AQUI PODEMOS MOSTRARLOS CON AYUDA DE OTRO FOR*/
CREATE OR REPLACE PROCEDURE insert_emps IS
    TYPE t_emps IS
        TABLE OF employees%rowtype INDEX BY BINARY_INTEGER;
    v_emptab t_emps;
BEGIN
    SELECT
        *
    BULK COLLECT
    INTO v_emptab
    FROM
        employees;

    FORALL i IN v_emptab.first..v_emptab.last SAVE EXCEPTIONS
        INSERT INTO employees VALUES v_emptab ( i );

EXCEPTION
    WHEN OTHERS THEN
        FOR j IN 1..SQL%bulk_exceptions.count LOOP
            dbms_output.put_line(SQL%bulk_exceptions(j).error_index);
            dbms_output.put_line(SQL%bulk_exceptions(j).error_code);
        END LOOP;
END insert_emps;

/*PUEDES HACER UN UPDATE DE UNA COLUMNA Y LUEGO ASIGNARLO EN UN SELECT A UNA NUEVA COLUMNA*/
CREATE OR REPLACE PROCEDURE update_one_emp (
    p_emp_id                IN  employees.employee_id%TYPE,
    p_salary_raise_percent  IN  NUMBER
) IS
    v_new_salary employees.salary%TYPE;
BEGIN
    UPDATE employees
    SET
        salary = salary * ( 1 + p_salary_raise_percent )
    WHERE
        employee_id = p_emp_id;

    SELECT
        salary
    INTO v_new_salary
    FROM
        employees
    WHERE
        employee_id = p_emp_id;

    dbms_output.put_line('New salary is: ' || v_new_salary);
END update_one_emp;
/*PERO PODEMOS HACERLO MAS FACIL CON UN RETURNING*/
CREATE OR REPLACE PROCEDURE update_one_emp (
    p_emp_id                IN  employees.employee_id%TYPE,
    p_salary_raise_percent  IN  NUMBER
) IS
    v_new_salary employees.salary%TYPE;
BEGIN
    UPDATE employees
    SET
        salary = salary * ( 1 + p_salary_raise_percent )
    WHERE
        employee_id = p_emp_id
    RETURNING salary INTO v_new_salary;

    dbms_output.put_line('New salary is: ' || v_new_salary);
END update_one_emp;

/*-------OTRO EJEMPLO-----*/
CREATE OR REPLACE PROCEDURE update_all_emps (
    p_salary_raise_percent IN NUMBER
) IS

    TYPE t_empid IS
        TABLE OF employees.employee_id%TYPE INDEX BY BINARY_INTEGER;
    TYPE t_sal IS
        TABLE OF employees.salary%TYPE INDEX BY BINARY_INTEGER;
    v_empidtab  t_empid;
    v_saltab    t_sal;
BEGIN
    SELECT
        employee_id
    BULK COLLECT
    INTO v_empidtab
    FROM
        employees;

    FORALL i IN v_empidtab.first..v_empidtab.last
        UPDATE employees
        SET
            salary = salary * ( 1 + p_salary_raise_percent )
        WHERE
            employee_id = v_empidtab(i);

    SELECT
        salary
    BULK COLLECT
    INTO v_saltab
    FROM
        employees;

END update_all_emps;


/*Y LA OTRA FORMA DE HACERLO*/
CREATE OR REPLACE PROCEDURE update_all_emps (
    p_salary_raise_percent IN NUMBER
) IS

    TYPE t_empid IS
        TABLE OF employees.employee_id%TYPE INDEX BY BINARY_INTEGER;
    TYPE t_sal IS
        TABLE OF employees.salary%TYPE INDEX BY BINARY_INTEGER;
    v_empidtab  t_empid;
    v_saltab    t_sal;
BEGIN
    SELECT
        employee_id
    BULK COLLECT
    INTO v_empidtab
    FROM
        employees;

    FORALL i IN v_empidtab.first..v_empidtab.last
        UPDATE employees
        SET
            salary = salary * ( 1 + p_salary_raise_percent )
        WHERE
            employee_id = v_empidtab(i)
        RETURNING salary BULK COLLECT INTO v_saltab;

END update_all_emps;