/****************************************************************/
/*****************************FOR LOOP***************************/
/****************************************************************/
/*HAY DOS MANERAS DE HACER UN CURSOR, LA PRIMERA ES CON LA SIGUIENTE SENTENCIA Y ORDEN, HACIENDO USO DE OPEN, FETCH Y CLOSE 
DECLARE CURSOR name_cursor
    SELECT *
    FROM table_name
    WHERE condition_to_select
  v_variable_con_tipo name_cursor%rowtype
  
BEGIN   
  OPEN cursor_name
    LOOP
        FETCH cursor_name INTO  v_variable_con_tipo
        EXIT WHEN condicion_para_salir
    END LOOP
  CLOSE cursor_name
END;  

/*LA OTRA FORMA DE HACERLO ES UN FOR LOOP

DECLARE 
    CURSOR cursor_name
        SELECT *
        FROM table_name
        WHERE condition_to_choose
BEGIN
    FOR v_variable_record IN cursor_name
        LOOP
        END LOOP
END;    
    
*/
/*****************--------------FOR LOOP-------------********************/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

BEGIN
    FOR v_emp_record IN cur_emps LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;
/*****************-----------OPEN,FETCH,INTO---------------********************/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;
    v_emp_record cur_emps%rowtype;
BEGIN
    OPEN cur_emps;
    LOOP
    FETCH  cur_emps INTO v_emp_record;
    EXIT WHEN cur_emps%notfound;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
    CLOSE cur_emps;
END;

/*****************------------------------------********************/

DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name
    FROM
        departments
    ORDER BY
        department_id;

BEGIN
    FOR v_dept_record IN cur_depts LOOP
        dbms_output.put_line(v_dept_record.department_id
                             || ' '
                             || v_dept_record.department_name);
    END LOOP;
END;

/*****************------------------------------********************/
/*AUNQUE YA NO TENGAMOS EL FETCH, OPEN Y LO DEMAS, AUN PODEMOS USAR %ROWCOUNT
Y LO PODEMOS USAR TAMBIEN COMO CONDICION*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees;

BEGIN
    FOR v_emp_record IN cur_emps LOOP
        EXIT WHEN cur_emps%rowcount > 5;
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;


/*------------------------------------------*/

/*SE PUEDEN USAR SUBQUERIES DENTRO DE UN FOR, SE DEBE DE PONER ENTRE PARENTESIS DESPUES DEL "IN"*/

BEGIN
    FOR v_emp_record IN (
        SELECT
            employee_id,
            last_name
        FROM
            employees
        WHERE
            department_id = 50
    ) LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;
/*------AHORA, LA SENTENCIA DE ARIIBA SE PUEDE ESCRIBIR DE DIFERENTE FORMA, PERO EN ESTE CODIGO SE ESCRIBE MUCHO MAS*/
DECLARE
    CURSOR cur_depts IS
    SELECT
        *
    FROM
        departments;

    v_dept_rec cur_depts%rowtype;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO v_dept_rec;
        EXIT WHEN cur_depts%notfound;
        dbms_output.put_line(v_dept_rec.department_name);
    END LOOP;

    CLOSE cur_depts;
END;